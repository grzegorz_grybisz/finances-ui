import React from 'react';

import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { SnackbarProvider } from 'notistack';

import Routes from './Routes';
import Notifier from './notifications/Notifier';
import NotificationProvider from './notifications/NotificationProvider';
import Theme from './Theme'

const App = () =>(
  <MuiPickersUtilsProvider utils={DateFnsUtils}>
    <Theme>
      <NotificationProvider>
        <Notifier/>
        <Routes />
      </NotificationProvider>
    </Theme>
  </MuiPickersUtilsProvider>
)

export default App;
