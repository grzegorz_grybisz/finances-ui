import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import LoginPage from './LoginPage';
import SecuredAppPage from './SecuredAppPage';
import AuthCallback from './auth/AuthCallback';
import SilentCallback from './auth/SilentCallback';


const Routes = (props) => {
  return (
    <Router>
      <Switch>
          <Route path="/login" component={LoginPage}/>
          <Route path="/callback" component={AuthCallback} />
          <Route path="/silentcallback" component={SilentCallback} />
          <Route path="/" component={SecuredAppPage}/>
      </Switch>
    </Router>
  );
}

export default Routes;
