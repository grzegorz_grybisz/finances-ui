import React, { Fragment } from 'react';

import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
	spash: {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  }
})

const SplashPage = (props) =>(
  <div className={props.classes.spash}>
    <CircularProgress size={100} />
  </div>
)

export default withStyles(styles)(SplashPage);
