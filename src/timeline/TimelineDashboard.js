import React from 'react';
import {Route} from 'react-router-dom';

import Grid from '@material-ui/core/Grid';

import TimelineChartWidget from './TimelineChartWidget';

export default () => (
    <div>
      <Grid container spacing={4} justify='center'>
        <Grid item item sm={12} md={12}>
            <Route path="/timeline" component={TimelineChartWidget}/>
        </Grid>
      </Grid>
    </div>
)
