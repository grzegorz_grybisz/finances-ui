import React from 'react';
import { connect } from 'react-redux'

import WidgetFrame from 'Components/Widget/WidgetFrame';
import { Chart, ArgumentAxis, ValueAxis, LineSeries } from "@devexpress/dx-react-chart-material-ui";

import { changeTransactionSearchText, fetchTransactions } from '../store/transactions/action'
import { groupByKey, groupBy } from '../util/ArrayUtils'

const frameConfig = {
  title: "Timeline",
}

const TimelineChartWidget = (props) => {
  return (
      <WidgetFrame config={frameConfig}>
        <Chart data={props.data}>
            <ArgumentAxis />
            <ValueAxis />
            <LineSeries valueField="value" argumentField="argument" />
        </Chart>
      </WidgetFrame>
  );
}

const mapTransactionsToChartData = (transactions) => {

  const groups = Object.entries(groupBy(transactions, 'date'));
  console.log(groups);
  //const data = groups.map(([key, v]) => { return {argument: key, value: v}});
  const data = groups.map(mapTransactionGroupToChartValue);
  console.log(data);
  //const data = groups.map(([key, v]) => {argument: key, value: v});
  //
  // Object.keys(groups).forEach((commonBrand) => {
  //   console.log(commonBrand);
  //   commonBrand.forEach((car) => {
  //     console.log('    ' + car.amount);
  //   });
  // });

//  const data = groups.map(mapTransactionGroupToChartValue);

  // var data = [];
  // for(var groupIndex in groups){
  //   if(groups.hasOwnProperty(groupIndex)){
  //     data.push(
  //       {
  //           argument: groupIndex,
  //           value: 10
  //       }
  //     )
  //   }
  // }

  // const data = groups.map(mapTransactionsToChartData);
  // console.log(data)
  return data;
}

const mapTransactionGroupToChartValue = ([date, v]) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const sumOfAmounts = v.map(i => i.amount).reduce(reducer);

  return {
    argument: date,
    value: sumOfAmounts
  }
}


//TODO use reselect library (later ;) )
const mapStateToProps = state =>{
  return {
    data: mapTransactionsToChartData(state.transactions.items),
    isFetching: state.transactions.isFetching,
  }
}

const mapDispatchToProps = dispatch => {
    dispatch(fetchTransactions());
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineChartWidget);
