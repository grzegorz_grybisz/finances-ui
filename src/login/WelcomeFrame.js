import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Fade from '@material-ui/core/Fade';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  frame: {
    padding: theme.spacing(2),
    backgroundColor: 'transparent',
    boxShadow: "none",
  },
  title: {
    color: theme.palette.common.white
  }
})

class WelcomeFrame extends React.Component {

  render(){
    return (
      <Fade in={true} timeout={1500}>
      <Paper className={this.props.classes.frame}>
        <Grid container direction='column' alignItems='center' spacing={40}>
          <Grid item>
            <Typography className={this.props.classes.title} variant="h2">
              FINANCES
            </Typography>
          </Grid>
          <Grid item>
            {this.props.children}
          </Grid>
        </Grid>
      </Paper>
      </Fade>
    );
  }
}

export default withStyles(styles)(WelcomeFrame);
