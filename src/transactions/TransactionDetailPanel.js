import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import DoneIcon from '@material-ui/icons/Done';

import TagChip from 'Components/TagChip/TagChip';

const styles = theme => ({
  root: {
    backgroundColor: theme.palette.action.selected
  },
  chip: {
    margin: theme.spacing(0.5),
  },
});

const TransactionDetailPanel = (props) =>{

const {classes, transaction} = props;

const tagsChips = generateTags(transaction.tags, classes);

  return (
    <Grid className={classes.root} container spacing={0}>
      <Grid item xs={12}>
        {tagsChips}
      </Grid>
    </Grid>
  );
}

const generateTags = (tags, classes) => {
  var chips = [];
  for(var index in tags){
      const tag = tags[index];
      chips.push( <TagChip key={index} label={tag}/>);
  }
  return chips;
}


export default withStyles(styles)(TransactionDetailPanel);
