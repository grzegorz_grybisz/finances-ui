import React from 'react';

import ContextAction from '../../common/actions/ContextAction';
import NewTransactionForm from '../form/NewTransactionForm';

import {
  EXPENSE
} from '../../util/transactionUtils'

const NewExpenseAction = (props) => {

  return(
    <ContextAction location={props.location}>
      <NewTransactionForm type={EXPENSE}/>
    </ ContextAction>
  );
}

export default NewExpenseAction;
