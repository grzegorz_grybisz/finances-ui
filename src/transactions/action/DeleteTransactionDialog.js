import React from 'react';
import { connect } from 'react-redux'

import { deleteTransaction } from '../../store/transactions/action.js'
import { notifySuccess } from '../../store/snackbars/action'

import DialogPopupContainer from "Components/DialogPopupContainer/DialogPopupContainer";
import AlertDialog from "Components/AlertDialog/AlertDialog";

class DeleteTransactionDialog extends React.Component{

  state = {
    isOpen: true,
    transactionId: this.props.match.params.transactionId
  }

  handleSubmit = (event) => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/transactions');
    this.props.onSubmit(this.state.transactionId);
  }

  handleClose = () => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/transactions');
  };

  render(){

    const config = {
      description: {
        title: "Delete transaction",
        content: "Are you sure you want to remove this transaction?"
      },
      buttonsConfig: [
          {
            id: 'cancel',
            label: 'Cancel',
            color: 'secondary',
            onClick: this.handleClose,
          },
          {
            id: 'ok',
            label: 'OK',
            color: 'primary',
            submit: true,
            onClick: this.handleSubmit,
          }
        ]
    }

    return(
      <DialogPopupContainer
        isOpen={this.state.isOpen}
        onClose={this.handleClose}
      >
        <AlertDialog config={config}/>
      </DialogPopupContainer>
      );
    }
}

const mapStateToProps = state => {
  return {
    state: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: transactionId => {
      dispatch(deleteTransaction(transactionId))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeleteTransactionDialog);
