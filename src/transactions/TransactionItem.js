import React, { Fragment } from 'react';

import HighlightedText from '../common/HighlightedText';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';

import AmountText from 'Components/AmountText/AmountText';
import ActionsPanel from 'Components/ActionsPanel/ActionsPanel'

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';

import { chooseRandomFrom, chooseRandomBetween } from '../util/randomUtils';
import { expensePalette, incomePalette } from '../themes/themes';

const TransactionItem = (props) =>{

  return (
      <ListItem button selected={props.isExpanded} onClick={props.onClick}>
        <Grid container alignItems='center'>
          <Grid item xs={4} sm={5} md={5}>
            <MainText {...props}/>
          </Grid>
          <Grid item xs={4} sm={4} md={4}>
            <Amount {...props}/>
          </Grid>
          <Grid item xs={4} sm={3} md={3}>
            <Actions {...props}/>
          </Grid>
         </Grid>
      </ListItem>
  );
}

const MainText = (props) =>{
  const {transaction, filterText} = props;

  if(transaction){
    return (<ListItemText secondary={transaction.category.name}>
              <HighlightedText text={transaction.name} highlight={filterText}/>
            </ListItemText>
          );
  }
  return <MainTextPlaceholder/>
}

const MainTextPlaceholder = () =>{
  const nameLength = 100;//chooseRandomBetween(50, 150);
  const descriptionLength = 200;//chooseRandomBetween(100, 250);
  return (<Grid container direction="column">
            <Skeleton width={nameLength}/>
            <Skeleton width={descriptionLength}/>
          </Grid>);
}

const Amount = (props) => {
  const {transaction} = props;

  if(transaction){
    return (
      <ListItemText>
              <AmountText text={transaction.amount} type={transaction.type}/>
            </ListItemText>);
  }

  const amountPalette = chooseRandomFrom([expensePalette, incomePalette]);
  return (<Grid container justify='flex-end'>
            <SkeletonTheme color={amountPalette.primary} highlightColor={amountPalette.highlight}>
              <Skeleton width={70} height={20}/>
            </SkeletonTheme>
          </Grid>);
}

const Actions = (props) =>{
  const {transaction} = props;

  if(transaction){
    const objectId = transaction.id;

    const config = {
      buttons: [
        {
          link: `/transactions/edit/${objectId}`,
          label: 'Edit',
          iconName: 'EDIT'
        },
        {
          link: `/transactions/delete/${objectId}`,
          label: 'Delete',
          iconName: 'DELETE'
        }
      ]
    }

    return (
      <ActionsPanel {...{config}}/>
    );
  }

  return (<Grid container justify='space-evenly'>
          <Skeleton circle={true} height={35} width={35}/>
          <Skeleton circle={true} height={35} width={35}/>
        </Grid>);
}

export default TransactionItem;
