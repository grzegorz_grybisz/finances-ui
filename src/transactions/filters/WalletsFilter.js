import React from 'react'
import { connect } from 'react-redux'

import ConfigurableList from 'Components/ConfigurableList/ConfigurableList';

import { fetchWallets } from '../../store/wallets/action';
import WalletFilterItem from './WalletFilterItem';

function WalletsFilter(props) {

  const state = {
    ...props.listSettings,
    listItemWrapper: WalletFilterItem
  }

  return (
      <ConfigurableList
        listConfig={state}>
        {props.wallets}
      </ ConfigurableList>
  );
}

const mapStateToProps = state => {
  return {
    wallets: state.wallets.items,
    isFetching: state.wallets.isFetching,
    listSettings: state.settings,
  }
}

const mapDispatchToProps = dispatch => {
  dispatch(fetchWallets());
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WalletsFilter);
