import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getFilterTags } from '../../store/selectors';
import { toggleFilterTag } from '../../store/filter/tagFilter/action';

import Grid from '@material-ui/core/Grid';
import TagChip from 'Components/TagChip/TagChip';

export default function TagsFilter() {
    const dispatch = useDispatch();
    //TODO move to selectors?
    const allTags = useSelector(state => state.tags.items);
    const selectedTags = useSelector(getFilterTags);

    const onClick = tagLabel => dispatch(toggleFilterTag(tagLabel));

    return (
        <Grid>
            {generateChips(allTags, selectedTags, onClick)}
        </Grid>
    );
}

const generateChips = (allTags, selectedTags, onClick) => {
    return allTags.map((tag, index) => {
        const isSelected = selectedTags.includes(tag);

        return <TagChip key={index} label={tag} onClick={() => onClick(tag)} isSelected={isSelected} />
    });
}