import React from 'react';
import { connect } from 'react-redux';

import WidgetFrame from 'Components/Widget/WidgetFrame';
import WalletsFilter from './WalletsFilter';
import TagsFilter from './TagsFilter';

function FiltersWidget(props) {

  const frameConfig = {
    title: "Filters"
  }

  return (
    <WidgetFrame config={frameConfig} isLoading={props.isFetching}>
      <WalletsFilter />
      <TagsFilter />
    </WidgetFrame>
  );
}

const mapStateToProps = state =>{
  return {
    wallets: state.wallets.items,
    isFetching: state.wallets.isFetching,
    listSettings: state.settings,
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersWidget);
