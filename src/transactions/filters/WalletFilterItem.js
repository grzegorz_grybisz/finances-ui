import React from 'react';

import AmountText from 'Components/AmountText/AmountText';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import { incomePalette } from '../../themes/themes';

const WalletItem = (props) =>{

  const {content, isChecked} = props;
  const wallet = content;

  return (
    <div>
      <ListItem button>
        <Grid container alignItems='center'>
          <Grid item xs={6}>
            <MainText wallet={wallet} />
          </Grid>
          <Grid item xs={4}>
            <Amount wallet={wallet} />
          </Grid>
          <Grid item xs={2}>
            <Switch
              checked={isChecked}
              color="primary"
              />
          </Grid>
         </Grid>
      </ListItem>
    </div>
  );
}

const MainText = (props) =>{
  const {wallet} = props;

  if(wallet){
    return (<ListItemText primary={wallet.name} />);
  }
  const nameLength = 50;
  return (<Skeleton width={nameLength}/>);
}


const Amount = (props) => {
  const {wallet} = props;

  if(wallet){
    return (<ListItemText>
              <AmountText text={wallet.amount} type='INCOME'/>
            </ListItemText>);
  }

  return (<Grid container justify='flex-end'>
            <SkeletonTheme color={incomePalette.primary} highlightColor={incomePalette.highlight}>
              <Skeleton width={70} height={20}/>
            </SkeletonTheme>
          </Grid>);
}

export default WalletItem;
