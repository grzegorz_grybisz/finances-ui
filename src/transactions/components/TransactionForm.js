import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import ConfigurableForm from 'Components/ConfigurableForm/ConfigurableForm';
import DialogPopupContainer from 'Components/DialogPopupContainer/DialogPopupContainer';

import {
  convertToDto,
  convertReduxStateToData,
} from '../../util/DtoUtils';


class TransactionForm extends React.Component{

  state = {
    isOpen: true,
  };

  handleSubmit = values => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/transactions');
    this.props.onSubmit(values);
  }

  handleClose = () => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/transactions');
  };

  render(){

    const {externalData, initialFormData} = this.props;
    const formValues = initialFormData

    const formConfig = {
        description: {
          title: this.props.title,
          content: this.props.content
        },
        formButtons: [
          {
            id: 'cancel',
            label: 'Cancel',
            color: 'secondary',
            onClick: this.handleClose,
          },
          {
            id: 'ok',
            label: 'OK',
            color: 'primary',
            submit: true
          }
        ],
        items: [
          {
            key: 'temp',
            xs: 12,
            fields: [
              {
                id: "template",
                label: "From template",
                autoFocus: false,
                selectItems: externalData.templatesData.options,
                autocomplete: true
              },
            ]
          },
          {
            key: 'name',
            xs: 12,
            sm: 8,
            fields: [
              {
                id: "name",
                label: "Name",
                autoFocus: true,
                size: 'large'
              },
            ]
          },
          {
            key: 'amount',
            xs: 12,
            sm: 4,
            fields: [
              {
                id: "amount",
                label: "Amount",
                autoFocus: false,
                amount: true,
                size: 'large'
              },
            ]
          },
          {
            key: 'cat',
            xs: 12,
            sm: 7,
            fields: [
              {
                id: "category",
                label: "Category",
                autoFocus: false,
                selectItems: externalData.categoriesData.items,
                autocomplete: true
              },
            ]
          },
          {
            key: 'tag',
            xs: 12,
            fields: [
              {
                id: "tags",
                label: "Tags",
                placeholder: 'Select multiple tags',
                autoFocus: false,
                selectItems: externalData.tagsData.items,
                multiselect: true
              },
            ]
          },
          {
            key: 'date',
            xs: 12,
            sm: 6,
            fields: [
              {
                id: "date",
                label: "Date",
                autoFocus: false,
                date: true
              },
            ]
          },
          {
            key: 'wallet',
            xs: 12,
            sm: 6,
            fields: [
              {
                id: "wallet",
                label: "Wallet",
                autoFocus: false,
                selectItems: externalData.walletsData.options,
                autocomplete: true
              },
            ]
          },
        ],
      }

    return(
      <DialogPopupContainer
        isOpen={this.state.isOpen}
        onClose={this.handleClose}
      >
        <ConfigurableForm onSubmit={this.handleSubmit} formConfig={formConfig} formValues={formValues}/>
      </DialogPopupContainer>
    );
  }
}

export default withRouter(TransactionForm);
