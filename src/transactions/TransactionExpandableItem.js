import React from 'react';

import Collapse from '@material-ui/core/Collapse';

import TransactionItem from './TransactionItem.js';
import TransactionDetailPanel from './TransactionDetailPanel.js';
import Divider from '@material-ui/core/Divider';

class TransactionExpandableItem extends React.Component{
  state = {
    expanded: false
  }

  handleExpandStateChange = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  }

  render(){

    const {content} = this.props;
    const transaction = content;
    const filterText = "";

    return (
      <div>
      <TransactionItem
        transaction={transaction}
        filterText={filterText}
        onClick={this.handleExpandStateChange}
        isExpanded={this.state.expanded}/>
      <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
        <TransactionDetailPanel transaction={transaction}/>
      </Collapse>
    </div>
    );
  }

}

export default TransactionExpandableItem;
