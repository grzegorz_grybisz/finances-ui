import React from 'react'
import { connect } from 'react-redux'

import { fetchTransactions } from '../store/transactions/action'
import { fetchCategories } from '../store/categories/CategoriesActions'
import { fetchTags } from '../store/tags/action'
import { getFilteredTransactions } from '../store/selectors';

import Grid from '@material-ui/core/Grid';

import ConfigurableList from 'Components/ConfigurableList/ConfigurableList';
import WidgetFrame from 'Components/Widget/WidgetFrame'
import TransactionExpandableItem from './TransactionExpandableItem.js';

class TransactionWidget extends React.Component{
  state = {
    groupBy: 'date',
    listItemWrapper: TransactionExpandableItem,
    ...this.props.listSettings
  }

  frameConfig = {
    title: "Transactions",
  }

  getFromState = name => {
    return this.state[name];
  }

  handleChange = name => event => {
    const currentValue = this.state[name];
    const newValue = !currentValue;

    this.setState({ [name]: newValue });
  };

  render(){
    const props = this.props;

    return (
      <WidgetFrame config={this.frameConfig} isLoading={this.props.isFetching}>
          <Grid container spacing={2} justify='center'>
            <Grid item xs={12}>
              <ConfigurableList
                listConfig={this.state}
                filterText={props.searchText}>
                {props.data}
              </ ConfigurableList>
            </Grid>
          </Grid>
      </WidgetFrame>
      );
      }
    }

//TODO use reselect library (later ;) )
const mapStateToProps = state =>{
  return {
    data: getFilteredTransactions(state),
    isFetching: state.transactions.isFetching,
    listSettings: state.settings,
  }
}

const mapDispatchToProps = dispatch => {
  dispatch(fetchTransactions());
  dispatch(fetchCategories());
  dispatch(fetchTags());
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionWidget);
