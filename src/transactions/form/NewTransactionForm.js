import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import { addTransaction, fetchTransactions } from '../../store/transactions/action'
import { updateAttributes } from '../../util/ArrayUtils'
import {
  EXPENSE
} from '../../util/transactionUtils'

import TransactionForm from '../components/TransactionForm';
import {
  convertToDto,
  convertReduxStateToData,
  findPreselectedOption,
} from '../../util/DtoUtils';

const NewTransactionForm = (props) => {
  const formData = prepareInitialFormData(props);
  const title = getFormTitle(props.type);

  return (
    <TransactionForm
      title={title}
      externalData={props.externalData}
      onSubmit={props.onSubmit}
      initialFormData={formData}/>
  );
}

const prepareInitialFormData = props => {

  const { externalData } = props;

  const defaultWallet = externalData.walletsData.defaultOption;
  const defaultDate = externalData.defaultDate;

  const initialState = {
    name: "",
    category: "",
    wallet: defaultWallet,
    tags: [],
    amount: "",
    date: defaultDate,
    type: props.type,
    template: ''
  }

  const template = getPreselectedTemplate(props);
  if(template){
    const modifiedAttr = {
      template: template,
      category: findPreselectedOption(externalData.categoriesData.items, template.transaction.category),
    }

    const attributesFromTemplate = updateAttributes(template.transaction, modifiedAttr);
    return updateAttributes(initialState, attributesFromTemplate);
  }
  return initialState;

}

const getPreselectedTemplate = props => {
  if(props.actionParams){
    return props.externalData.templatesData.options.find(opt => opt.value == props.actionParams.templateId);
  }
  return null;
}

const getFormTitle = type => {
  if(type === EXPENSE){
    return "New expense"
  }
  return "New income"
}

const mapStateToProps = state => {
  return {
    externalData: convertReduxStateToData(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: formData => {
      const dto = convertToDto(formData)
      //alert(JSON.stringify(dto))
      dispatch(addTransaction(dto))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTransactionForm);
