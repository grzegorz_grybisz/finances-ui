import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import { updateTransaction } from '../../store/transactions/action.js'
import { notifySuccess } from '../../store/snackbars/action'

import TransactionForm from '../components/TransactionForm.js';
import { convertStringToDate } from '../../util/DateUtils'
import {
  convertToDto,
  convertReduxStateToData,
  convertTagsToSelectOptions,
  findPreselectedOption,
} from '../../util/DtoUtils';

const EditTransactionForm = (props) => {
  const editTransaction = props.transactions.find(transaction => {
    return transaction.id == props.match.params.transactionId;
  })

  const formData = prepareInitialFormData(editTransaction, props.externalData);
  const title = getFormTitle(isExpense(editTransaction));

  return (
    <TransactionForm
      title={title}
      externalData={props.externalData}
      onSubmit={props.onSubmit}
      initialFormData={formData}/>
  );
}

const prepareInitialFormData = (editTransaction, externalData) => {
  return {
      id: editTransaction.id,
      name: editTransaction.name,
      category: findPreselectedOption(externalData.categoriesData.items, editTransaction.category.id),
      wallet: findPreselectedOption(externalData.walletsData.options, editTransaction.wallet.id),
      tags: convertTagsToSelectOptions(editTransaction.tags),
      amount: editTransaction.amount,
      date: convertStringToDate(editTransaction.date),
      type: editTransaction.type
    }
}

const isExpense = editTransaction => {
  return editTransaction.type == 'EXPENSE';
}

const getFormTitle = isExpense => {
  if(isExpense){
    return "Edit expense"
  }
  return "Edit income"
}

const mapStateToProps = state => {
  return {
    transactions: state.transactions.items,
    externalData: convertReduxStateToData(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: formData => {
      const dto = convertToDto(formData)
      dispatch(updateTransaction(formData.id, dto))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTransactionForm);
