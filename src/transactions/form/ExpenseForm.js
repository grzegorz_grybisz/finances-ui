import React from 'react';

import NewTransactionForm from './NewTransactionForm.js';

import {
  EXPENSE
} from '../../util/transactionUtils'


const ExpenseForm = (props) => {

  return(
    <NewTransactionForm type={EXPENSE}/>
  );
}

export default ExpenseForm;
