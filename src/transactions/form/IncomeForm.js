import React from 'react';

import NewTransactionForm from './NewTransactionForm.js';

import {
  INCOME
} from '../../util/transactionUtils'


const IncomeForm = () => (
       <NewTransactionForm type={INCOME}/>
     )

export default IncomeForm;
