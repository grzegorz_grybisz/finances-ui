import React from 'react';

import HighlightedText from '../common/HighlightedText.js';
import AmountText from '../common/AmountText.js';
import EditButton from '../common/EditButton.js';
import DeleteButton from '../common/DeleteButton.js';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  card: {
  }
});

const TransactionCardItem = (props) =>{

  const {classes, transaction, filterText} = props;

  return (
      <Card className={classes.card}>
        <CardActionArea onClick={props.onClick}>
          <CardContent>
            <Grid container
              className={classes.root}
              direction="row"
              justify="space-between"
              alignItems="center"
              >
                <Grid item xs={2}>
                  <Typography color="default">
                    <HighlightedText text={transaction.name} highlight={filterText}/>
                  </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography>
                    <AmountText text={transaction.amount} type={transaction.type}/>
                  </Typography>
                </Grid>
                <Grid container>
                  <Typography color="textSecondary">
                    {transaction.category.name}
                  </Typography>
                </Grid>
              </Grid>
            </CardContent>
                      </CardActionArea>
            <CardActions disableActionSpacing>
              <EditButton objectId={transaction.id}/>
              <DeleteButton objectId={transaction.id}/>
            </CardActions>

        </Card>
    );
  }

  export default withStyles(styles)(TransactionCardItem);
