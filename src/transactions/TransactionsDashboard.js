import React from 'react';
import {Route} from 'react-router-dom';

import Grid from '@material-ui/core/Grid';

import IncomeForm from './form/IncomeForm';
import NewExpenseAction from './action/NewExpenseAction';
import EditTransactionForm from './form/EditTransactionForm';
import DeleteTransactionDialog from './action/DeleteTransactionDialog';
import TransactionWidget from './TransactionsWidget';
import TransactionButtons from './TransactionButtons';
import FiltersWidget from './filters/FiltersWidget';
import BalanceWidget from 'Components/BalanceWidget/BalanceWidget';

const TransactionsDashboard = () => (
      <div>
      <Grid container spacing={4} justify='center'>
        <Grid item item sm={12} md={7}>
            <Route path="/transactions" component={TransactionWidget}/>
            <Route path="/transactions/income"  component={IncomeForm}/>
            <Route path="/transactions/expense" component={NewExpenseAction}/>
            <Route path="/transactions/edit/:transactionId" component={EditTransactionForm}/>
            <Route path="/transactions/delete/:transactionId" component={DeleteTransactionDialog}/>
        </Grid>
        <Grid item item sm={12} md={5}>
          <FiltersWidget />
        </Grid>
      </Grid>
      <TransactionButtons />
    </div>
)

//For future reference
//  <Route path="/transactions/income"  render={(props) => <IncomeForm {...props}/>}/>

export default TransactionsDashboard;
