import React from 'react'
import { connect } from 'react-redux'

import WidgetFrame from 'Components/Widget/WidgetFrame'
import Grid from '@material-ui/core/Grid';

import { changeSetting } from '../store/settings/action'
import CheckboxField from 'Components/CheckboxField/CheckboxField';

//TODO does this component needs own state, if everything is passed from redux store?
class SettingsWidget extends React.Component{
  state = {
      isDense: this.props.isDense,
      hasItemDividers: this.props.hasItemDividers,
      hasThickDividers: this.props.hasThickDividers,
      settingsItems: [
        {
          key: 'isDense',
          label: 'Dense',
          checkbox: true,
          dynamicValue: () => this.getFromState('isDense'),
          onClick: event => this.handleChange('isDense')(event)
        },
        {
          key: 'hasItemDividers',
          label: 'Item dividers',
          checkbox: true,
          dynamicValue: () => this.getFromState('hasItemDividers'),
          onClick: event => this.handleChange('hasItemDividers')(event)
        },
        {
          key: 'hasThickDividers',
          label: 'Bold Section dividers',
          checkbox: true,
          dynamicValue: () => this.getFromState('hasThickDividers'),
          onClick: event => this.handleChange('hasThickDividers')(event)
        }
      ]
  }

  frameConfig = {
    title: "Settings",
  }

  getFromState = name => {
    return this.state[name];
  }

  handleChange = name => event => {
    const currentValue = this.state[name];
    const newValue = !currentValue;

    this.setState({ [name]: newValue });
    this.props.onSettingChange(name, newValue);
  };

  render(){

    return (
      <WidgetFrame config={this.frameConfig} isLoading={this.props.isFetching}>
        <CheckboxField fieldConfig={this.state.settingsItems[0]}/>
        <CheckboxField fieldConfig={this.state.settingsItems[1]}/>
        <CheckboxField fieldConfig={this.state.settingsItems[2]}/>
      </WidgetFrame>
    );
  }
}

const mapStateToProps = state =>{
  const { settings } = state;

  return {
    isDense: settings.isDense,
    hasItemDividers: settings.hasItemDividers,
    hasThickDividers: settings.hasThickDividers,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSettingChange: (key, value) => {
      dispatch(changeSetting(key, value))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsWidget);
