import React from 'react';
import {Route} from 'react-router-dom';

import Grid from '@material-ui/core/Grid';

import SettingsWidget from './SettingsWidget';

const SettingsDashboard = () => (
      <div>
      <Grid container spacing={4} justify='center'>
        <Grid item item xs={12}>
            <Route path="/settings" component={SettingsWidget}/>
        </Grid>
      </Grid>
    </div>

)

export default SettingsDashboard;
