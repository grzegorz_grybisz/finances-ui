import React from 'react';

import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import { defaultTheme } from './themes/themes.js';

const Theme = (props) =>(
   <React.Fragment>
     <CssBaseline/>
  	 <MuiThemeProvider theme={defaultTheme}>
       {props.children}
  	 </MuiThemeProvider>
   </React.Fragment>
)

export default Theme;
