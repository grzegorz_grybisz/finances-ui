import React from 'react'
import { connect } from 'react-redux'
import { getFilteredTemplates, getSearchText } from '../store/selectors';

import ConfigurableList from 'Components/ConfigurableList/ConfigurableList';
import WidgetFrame from 'Components/Widget/WidgetFrame'

import { fetchTemplates } from '../store/templates/action'
import TemplateListItem from './TemplateListItem'


class TemplatesWidget extends React.Component{
  state = {
      ...this.props.listSettings,
      listItemWrapper: TemplateListItem
  }

  frameConfig = {
    title: "Templates",
  }

  getFromState = name => {
    return this.state[name];
  }

  handleChange = name => event => {
    const currentValue = this.state[name];
    const newValue = !currentValue;

    this.setState({ [name]: newValue });
  };

  render(){

    return (
      <WidgetFrame config={this.frameConfig} isLoading={this.props.isFetching}>
        <ConfigurableList 
          listConfig={this.state}
          filterText={this.props.searchText}>
          {this.props.items}
        </ConfigurableList>
      </WidgetFrame>
    );
  }
}

const mapStateToProps = state =>{
  return {
    items: getFilteredTemplates(state),
    isFetching: state.templates.isFetching,
    listSettings: state.settings,
    searchText: getSearchText(state),
  }
}

const mapDispatchToProps = dispatch => {
  dispatch(fetchTemplates());
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplatesWidget);
