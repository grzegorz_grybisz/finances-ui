import React from 'react';
import {Route} from 'react-router-dom';

import Grid from '@material-ui/core/Grid';

import TemplatesWidget from './TemplatesWidget';
import NewTemplateForm from './NewTemplateForm';
import TemplateDeleteDialog from './components/TemplateDeleteAction/TemplateDeleteDialog';
import TemplatesFabs from './components/TemplatesFabs/TemplatesFabs'

const TemplatesDashboard = () => (
      <div>
      <Grid container spacing={4} justify='center'>
        <Grid item sm={12} md={7}>
            <Route path="/templates" component={TemplatesWidget}/>
            <Route path="/templates/new" component={NewTemplateForm}/>
            <Route path="/templates/delete/:id" component={TemplateDeleteDialog}/>
        </Grid>
      </Grid>
      <TemplatesFabs />
    </div>

)
export default TemplatesDashboard;
