import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import { addTemplate, fetchTemplates } from '../store/templates/action'
import {
  EXPENSE
} from '../util/transactionUtils'

import TemplateForm from './components/TemplateForm/TemplateForm';

import {
  convertSelectedTagsToArray,
  convertReduxStateToData,
} from '../util/DtoUtils';

const NewTemplateForm = (props) => {
  const formData = prepareInitialFormData(props);
  const title = getFormTitle(EXPENSE);//props.type);

  return (
    <TemplateForm
      title={title}
      externalData={props.externalData}
      onSubmit={props.onSubmit}
      initialFormData={formData}/>
  );
}

const prepareInitialFormData = props => {
  const initialState = {
    name: "",
    category: "",
    tags: [],
    amount: "",
    type: {value: EXPENSE, label: 'Expense'},
  }

  return initialState;
}

const getFormTitle = type => {
  if(type === EXPENSE){
    return "New expense template"
  }
  return "New income template"
}

const mapStateToProps = state => {
  return {
    externalData: convertReduxStateToData(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: formData => {
      const dto = convertToDto(formData)
      dispatch(addTemplate(dto))
    }
  }
}

const convertToDto = formData => {
  return {
    name: formData.name,
    transaction: {
      name: formData.name,
      amount: formData.amount,
      category: formData.category.value,
      type: formData.type.value,
      tags: convertSelectedTagsToArray(formData.tags)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewTemplateForm);
