import React, { Fragment } from 'react';

import HighlightedText from '../common/HighlightedText';
import ExpendablePane from '../common/ExpendablePane';
import AmountText from 'Components/AmountText/AmountText';
import ActionsPanel from 'Components/ActionsPanel/ActionsPanel'

import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Collapse from '@material-ui/core/Collapse';
import Grid from '@material-ui/core/Grid';

import Skeleton, { SkeletonTheme } from 'react-loading-skeleton';
import { chooseRandomFrom, chooseRandomBetween } from '../util/randomUtils';
import { expensePalette, incomePalette } from '../themes/themes';

const TemplateListItem = (props) =>{

  const {classes, content, filterText} = props;
  const template = content;

  return (
    <ListItem button onClick={props.onClick}>
      <Grid container alignItems='center'>
        <Grid item xs={6}>
          <MainText template={template}/>
        </Grid>
        <Grid item xs={4}>
          <Amount template={template}/>
        </Grid>
        <Grid item xs={2}>
          <Actions template={template}/>
        </Grid>
       </Grid>
    </ListItem>
  );
}


const MainText = (props) =>{
  const {template} = props;

  if(template){
    return (<ListItemText primary={template.name} secondary={template.transaction.name} />);
  }

  const nameLength = 50;//chooseRandomBetween(50, 150);
  const descriptionLength = 100;//chooseRandomBetween(100, 250);
  return (<Grid container direction="column">
            <Skeleton width={nameLength}/>
            <Skeleton width={descriptionLength}/>
          </Grid>);
}

const Amount = (props) => {
  const {template} = props;

  if(template){
    return (<ListItemText>
              <AmountText text={template.transaction.amount} type={template.transaction.type}/>
            </ListItemText>);
  }

  const amountPalette = chooseRandomFrom([expensePalette, incomePalette]);
  return (<Grid container justify='flex-end'>
            <SkeletonTheme color={amountPalette.primary} highlightColor={amountPalette.highlight}>
              <Skeleton width={70} height={20}/>
            </SkeletonTheme>
          </Grid>);
}

const Actions = (props) =>{
  const {template} = props;

  if(template){
    const objectId = template.id;

    const config = {
      buttons: [
        {
          link: `/transactions/expense`,
          queryParams: {
            templateId: objectId
          },
          label: 'Create Transaction',
          iconName: 'FROM_TEMPLATE'
        },
        // {
        //   link:  `/templates/edit/${objectId}`,
        //   label: 'Edit',
        //   iconName: 'EDIT'
        // },
        {
          link: `/templates/delete/${objectId}`,
          label: 'Delete',
          iconName: 'DELETE'
        }
      ]
    }

    return (
      <ActionsPanel {...{config}}/>
    );
  }

  return (<Grid container justify='space-evenly'>
          <Skeleton circle={true} height={35} width={35}/>
          <Skeleton circle={true} height={35} width={35}/>
        </Grid>);
}

export default TemplateListItem;
