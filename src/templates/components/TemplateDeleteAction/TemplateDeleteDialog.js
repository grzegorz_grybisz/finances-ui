import React from 'react';
import { connect } from 'react-redux'

//import { deleteTransaction } from '../../store/transactions/action.js'
import { notifySuccess } from '../../../store/snackbars/action'

import DialogPopupContainer from "Components/DialogPopupContainer/DialogPopupContainer";
import AlertDialog from "Components/AlertDialog/AlertDialog";

class TemplateDeleteDialog extends React.Component{

  state = {
    isOpen: true,
    templateId: this.props.match.params.id
  }

  handleSubmit = (event) => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/templates');
    this.props.onSubmit(this.state.templateId);
  }

  handleClose = () => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/templates');
  };

  render(){

    const config = {
      description: {
        title: "Delete template",
        content: "Are you sure you want to remove this template?"
      },
      buttonsConfig: [
          {
            id: 'cancel',
            label: 'Cancel',
            color: 'secondary',
            onClick: this.handleClose,
          },
          {
            id: 'ok',
            label: 'OK',
            color: 'primary',
            submit: true,
            onClick: this.handleSubmit,
          }
        ]
    }

    return(
      <DialogPopupContainer
        isOpen={this.state.isOpen}
        onClose={this.handleClose}
      >
        <AlertDialog config={config}/>
      </DialogPopupContainer>
      );
    }
}

const mapStateToProps = state => {
  return {
    state: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmit: templateId => {
      alert(templateId);
      //dispatch(deleteTransaction(transactionId))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplateDeleteDialog);
