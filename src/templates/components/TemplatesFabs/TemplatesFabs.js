import React from 'react';
import { Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import Zoom from '@material-ui/core/Zoom';

const styles = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing(4),
    right: theme.spacing(4),
  },
	new: {
		background: theme.palette.primary.main,
		margin: 10
	},
  link: {
     textDecoration: 'none'
  }
});

const TemplatesFabs = (props) => {
 	return (
    <div className={props.classes.fab}>
    <Zoom in={true} unmountOnExit>
      <Grid container direction="column">
        <Link to="/templates/new" className={props.classes.link}>
            <Fab className={props.classes.new}>
              <AddIcon/>
            </Fab>
        </Link>
      </Grid>
    </Zoom>
    </div>
  );
}

export default withStyles(styles)(TemplatesFabs);
