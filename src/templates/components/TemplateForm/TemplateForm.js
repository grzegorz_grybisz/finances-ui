import React from 'react';
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom';

import ConfigurableForm from 'Components/ConfigurableForm/ConfigurableForm';
import DialogPopupContainer from 'Components/DialogPopupContainer/DialogPopupContainer';

import { EXPENSE, INCOME } from '../../../util/transactionUtils'

class TemplateForm extends React.Component{

  state = {
    isOpen: true,
  };

  handleSubmit = values => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/templates');
    this.props.onSubmit(values);
  }

  handleClose = () => {
    this.state = {...this.state, isOpen: false};
    this.props.history.push('/templates');
  };

  render(){

    const {externalData, initialFormData} = this.props;
    const formValues = initialFormData

    const formConfig = {
        description: {
          title: this.props.title,
          content: this.props.content
        },
        formButtons: [
          {
            id: 'cancel',
            label: 'Cancel',
            color: 'secondary',
            onClick: this.handleClose,
          },
          {
            id: 'ok',
            label: 'OK',
            color: 'primary',
            submit: true
          }
        ],
        items: [
          {
            md: 7,
            fields: [
              {
                id: 'type',
                label: 'Direction',
                autoFocus: true,
                select: true,
                selectItems: [
                  {
                    value: EXPENSE,
                    label: 'Expense'
                  },
                  {
                    value: INCOME,
                    label: 'Income'
                  },
                ]
              },
            ]
          },
          {
            xs: 12,
            sm: 8,
            fields: [
              {
                id: "name",
                label: "Name",
                autoFocus: true,
                size: 'large'
              },
            ]
          },
          {
            xs: 12,
            sm: 4,
            fields: [
              {
                id: "amount",
                label: "Amount",
                autoFocus: false,
                amount: true,
                size: 'large'
              },
            ]
          },
          {
            xs: 12,
            sm: 7,
            fields: [
              {
                id: "category",
                label: "Category",
                autoFocus: false,
                selectItems: externalData.categoriesData.items,
                autocomplete: true
              },
            ]
          },
          {
            xs: 12,
            fields: [
              {
                id: "tags",
                label: "Tags",
                placeholder: 'Select multiple tags',
                autoFocus: false,
                selectItems: externalData.tagsData.items,
                multiselect: true
              },
            ]
          },
        ]
      }

    return(
      <DialogPopupContainer
        isOpen={this.state.isOpen}
        onClose={this.handleClose}
      >
        <ConfigurableForm onSubmit={this.handleSubmit} formConfig={formConfig} formValues={formValues}/>
      </DialogPopupContainer>
    );
  }
}

export default withRouter(TemplateForm);
