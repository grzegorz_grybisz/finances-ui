import React from 'react';
import queryString from 'query-string'

const ContextAction = (props) => {
  const { children, location } = props;

  const actionParams = queryString.parse(location.search);

  return(
    <React.Fragment>{React.cloneElement(children, {actionParams: actionParams})}</React.Fragment>
  );
}

export default ContextAction;
