import React from 'react';

import { withTheme } from '@material-ui/core/styles'

const HighlightedText = (props) =>{
  const {text, highlight, theme} = props;
  const highlightColor = theme.palette.primary.light;

  if(shouldUseHighlight(highlight)){
    return getTextWithHighlight(text, highlight, highlightColor);
  }
  return getNormalText(text);
}

const shouldUseHighlight = textToHighlight => {
  return textToHighlight.length != 0;
}

const getNormalText = text =>(
  <span style={{whiteSpace: "pre"}}>{text}</span>
)

const getTextWithHighlight = (text, textToHighlight, highlightColor) => {
  var regex = new RegExp("(" + textToHighlight + ")", "gi");
  var parts = text.split(regex);

  return parts.map((part, index) => {
      if(regex.test(part)){
        return getHighlightedText(index, part, highlightColor);
      }
      return getNormalText(part);
    });
}

const getHighlightedText = (index, text, highlightColor) => (
    <mark key={index} style={{backgroundColor: highlightColor, whiteSpace: "pre"}}>{text}</mark>
)


export default withTheme(HighlightedText);
