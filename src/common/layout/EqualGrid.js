import React from 'react';

import Grid from '@material-ui/core/Grid';

const EqualGrid = props => {
  const { children } = props;

  const gridItems = wrapChildrenInGridItems(children);

  return(
    <Grid container spacing={5}>
      {gridItems}
    </Grid>
  );
}

const wrapChildrenInGridItems = children => {
  return children.map(function(child, index) {
    return (
      <Grid key={index} item xs={6}>
        {child}
      </Grid>
    )
  });
}

export default EqualGrid;
