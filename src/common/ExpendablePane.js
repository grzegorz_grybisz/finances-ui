import React from 'react';

import Collapse from '@material-ui/core/Collapse';

class ExpendablePane extends React.Component{
  constructor(props){
    super(props);

    this.state = { expanded: true };

  }

  onChangeExpandedState(){
    this.setState(state => ({ expanded: !state.expanded }));
  }

  render(){
    return (
      <div>
      {this.props.children}
      <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
        {this.props.expandable}
      </Collapse>
      </div>
    );
  }

}

export default ExpendablePane;
