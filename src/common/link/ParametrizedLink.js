import React from 'react';
import { Link } from 'react-router-dom';

import queryString from 'query-string';


const ParametrizedLink = (props) => {
  const { url, queryParams, children } = props;

  const fullUrl = addParamsToUrl(url, queryParams);

 	return (
    <Link to={fullUrl} style={{ textDecoration: 'none' }}>
      { children }
    </Link>
  );
}

const addParamsToUrl = (url, queryParams) => {
  if(queryParams){
    return `${url}?${queryString.stringify(queryParams)}`;
  }
  return url;
}

export default ParametrizedLink;
