import React, { Fragment } from 'react';
import { Route, Redirect } from 'react-router'
import Keycloak from 'keycloak-js';
import KeycloakConfig from '../keycloak.json'

import { connect } from "react-redux";

import SplashPage from './SplashPage';
import AppPage from './AppPage';
import LoginPage from './LoginPage';
import userManager from "./auth/UserManager";

class SecuredAppPage extends  React.Component {

  constructor(props) {
    super(props);

  }
 render(){
   const {isLoadingUser, user} = this.props;

  if(isLoadingUser){
    return <SplashPage/>;
  }else{
     if(!user || user.expired){
       console.log("Redirect to login ");
       return <Redirect to='/login'/>;
     }
    console.log("Logged in, no redirect");
    return <AppPage/>;
   }
 }
}

const mapStateToProps = state => {
  return {
    isLoadingUser: state.oidc.isLoadingUser,
    user: state.oidc.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SecuredAppPage);
