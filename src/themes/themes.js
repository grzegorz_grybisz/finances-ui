import { createMuiTheme } from '@material-ui/core/styles';

export const defaultTheme = createMuiTheme({
  palette: {
  //  type: 'dark',
    primary: {
      main: '#00b0ff',
      contrastText: '#ffffff'//'#000000',
    },
    background: {
      default: "#f0f0f0",
    },
  },
  typography: {
    useNextVariants: true,
  },
  props: {
    MuiPaper: {
      square: true,
    },
  },
});

export const primaryBackgroundTheme = createMuiTheme({
  overrides: {
   MuiGrid: {
     item: {
       backgroundColor: defaultTheme.palette.primary.main,
      '& *': { color: defaultTheme.palette.primary.contrastText },
     },
   },
 },
  palette: {
    action: {
      hover: defaultTheme.palette.primary.light,
    },
    divider: defaultTheme.palette.primary.dark,
  },
});

export const sidebarTheme = createMuiTheme({
  overrides: {
   MuiDrawer: {
     paper: {
       background: '#263238',
       '& *': { color: 'rgba(255, 255, 255, 0.8)' },
     },
   },
 },
  palette: {
    action: {
      hover: '#4f5b62',
    },
    divider: '#4f5b62',
  },
});

export const incomePalette = {
  primary: '#ddfbdd',
  highlight: '#ecfcec',
}

export const expensePalette = {
  primary: '#ffdddd',
  highlight: '#ffe7e7',
}
