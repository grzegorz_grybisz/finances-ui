import { getDeploymentLocation } from '../util/EnvironmentUtils'

describe('Deployment location', () => {
  describe('should be defined', () => {
    test('when variable DEPLOYED_ON is set', () => {

      const DEPLOYED_ON = 'local';

      const result = getDeploymentLocation(DEPLOYED_ON);
      expect(result).toEqual('local');
    });
  });

  describe('should be unknown', () => {
    test('when variable DEPLOYED_ON is null', () => {
        const DEPLOYED_ON = null;

        const result = getDeploymentLocation(DEPLOYED_ON);
        expect(result).toEqual('UNKNOWN');
    });

    test('when variable DEPLOYED_ON is undefined', () => {
        const DEPLOYED_ON = undefined;

        const result = getDeploymentLocation(DEPLOYED_ON);
        expect(result).toEqual('UNKNOWN');
    });
  });
});
