import { convertDateToString, convertStringToDate, getRelativeTime } from '../util/DateUtils'

const january = 0;
const december = 11;

test('convert Date (year, month, day) to string', () => {
  const date = new Date(2019, december, 17);

  const dateAsString = convertDateToString(date);

  expect(dateAsString).toBe("2019-12-17");
});

test('convert Date to string when month or day has single digit', () => {
  const date = new Date(2019, january, 5);

  const dateAsString = convertDateToString(date);

  expect(dateAsString).toBe("2019-01-05");
});

test('convert String (YY-MM-DD) to Date with zero day time', () => {
  const dateAsString = "2019-12-30";

  const date = convertStringToDate(dateAsString);

  expect(date).toEqual(new Date(2019, december, 30, 0, 0, 0, 0));
});

test('convert NULL to Date returns current time', () => {
  const date = convertStringToDate(null);

  const currentDate = new Date();

  expect(date.getFullYear()).toEqual(currentDate.getFullYear());
  expect(date.getMonth()).toEqual(currentDate.getMonth());
  expect(date.getDate()).toEqual(currentDate.getDate());
});
//
// test('get difference between two dates as string', () => {
//   const firstDate = new Date(2019, january, 10, 10, 10);
//   const secondDate = new Date(2019, january, 10, 10, 15);
//
//   const relativeTime = getRelativeTime(firstDate, secondDate);
//   expect(relativeTime).toBe("5 minutes");
// })
