import { groupBy, updateAttributes } from '../util/ArrayUtils'

test.skip('groupBy groups list items by key', () => {

  var originalArray = [
    {
      category: 'Food',
      name: 'Bread'
    },
    {
      category: 'Food',
      name: 'Eggs'
    },
    {
      category: 'Clothes',
      name: 'Shirt'
    },
  ];

  const result = groupBy(originalArray, 'category');
  //expect(resultString).toEqual(expected);
});


test('should update object with new attribute values', () => {

  const original = {
    attributeOne: 'one',
    attributeTwo: 'two',
    attributeThree: 'three'
  }

  const newValues = {
    attributeOne: 'updatedOne',
    attributeThree: 'updatedThree'
  }

  const result = updateAttributes(original, newValues);

  expect(result).toEqual({
    attributeOne: 'updatedOne',
    attributeTwo: 'two',
    attributeThree: 'updatedThree'
  })

});
