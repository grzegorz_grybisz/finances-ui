import { convertToDto, convertReduxStateToData, findPreselectedOption } from '../util/DtoUtils'

const defaultReduxState = {
  categories: { items: [] },
  tags: { items: [] },
  wallets: { items: [] },
  templates: { items: [] },
  transactions: { items: [], lastCreatedOn: '2019-02-01'}
}

describe('convert', () => {

  describe('to Dto', () => {
    test('Form Data', () => {
      const formData = {
        name: 'Transaction',
        amount: '10.1',
        category: {
          label: 'category',
          value: '123456'
        },
        type: 'INCOME',
        date: new Date(2019, 11, 20),
        tags: [
          {value: 'tag1Name', label: 'tag1Label'},
          {value: 'tag1Name', label: 'tag2Label'}
        ],
        wallet: {value: '3', label: 'walletName'}
      };

      const dto = convertToDto(formData);

      expect(dto).toEqual({
        name: 'Transaction',
        amount: '10.1',
        category: '123456',
        type: 'INCOME',
        date: '2019-12-20',
        tags: ['tag1Name', 'tag1Name'],
        wallet: '3'
      });
    });
  });

  describe('to Form select options', () => {
    test('Wallets state', () => {

      const state = {
        ...defaultReduxState,
        wallets: {
          defaultWallet: {
            name: 'Default',
            id: '1'
          },
          items: [
            {
              name: 'Account',
              id: '11'
            },
            {
              name: 'Wallet',
              id: '12'
            }
          ]
        }
      }

     const selectData = convertReduxStateToData(state);

     expect(selectData.walletsData).toEqual(
       {
         defaultOption: {
           label: 'Default',
           value: '1'
         },
         options: [
           {
             label: 'Account',
             value: '11'
           },
           {
             label: 'Wallet',
             value: '12'
           }
         ]
       }
     );
    });


    test('Tags state', () => {

      const state = {
        ...defaultReduxState,
        tags: {
          items: ['tag1', 'tag2']
        }
      }

     const selectData = convertReduxStateToData(state);

     expect(selectData.tagsData).toEqual(
       {
         items: [
           {
             label: 'tag1',
             value: 'tag1'
           },
           {
             label: 'tag2',
             value: 'tag2'
           }
         ]
       }
     );
    });


    test('Categories state', () => {

      const state = {
        ...defaultReduxState,
        categories: {
          items: [
            {
              id: '1234',
              name: 'Shopping',
              categories: [
                {
                  id: '34',
                  name: 'Clothes',
                },
                {
                  id: '56',
                  name: 'Food',
                }
              ]
            }
          ] }
      }

     const selectData = convertReduxStateToData(state);

     expect(selectData.categoriesData).toEqual(
       { items:
         [
           {
             label: "Shopping",
             options: [
               {
                 label: "Clothes",
                 value: "34"
               },
               {
                 label: "Food",
                 value: "56"
               }
             ]
           }
         ]
       }
     );
    });

    test('Templates state', () => {

      const state = {
        ...defaultReduxState,
        templates: {
          items: [
            {
              id: "1",
              name: "Template1",
              transaction: {
                amount: 1,
                tags: ["tag1", "tag2"]
              }
            },
            {
              id: "2",
              name: "Template2",
              transaction: {
                amount: 2,
                tags: []
              }
            }
          ]
         }
      };

     const selectData = convertReduxStateToData(state);

     expect(selectData.templatesData).toEqual(
       {
         options: [
           {
             label: 'Template1',
             value: '1',
             transaction: {
               amount: 1,
               tags: [
                 {
                   label: "tag1",
                   value: "tag1"
                 },
                 {
                   label: "tag2",
                   value: "tag2"
                 }
               ]
             }
           },
           {
             label: 'Template2',
             value: '2',
             transaction: {
               amount: 2,
               tags: []
             }
           }
         ]
       }
     );
    });
  });
});

test('should extract date of last creation from state', () => {
   const selectData = convertReduxStateToData(defaultReduxState);

   expect(selectData.defaultDate).toEqual(new Date(2019, 1, 1));
});

describe('select option', () => {
  const options = [
    {
       label: "Shopping",
       options: [
         {
           label: "Clothes",
           value: "34"
         },
         {
           label: "Food",
           value: "56"
         }
       ]
     }
   ]

   test('should be found by value when matching option is present', () => {
     const option = findPreselectedOption(options, '56');

     expect(option).toEqual(
       {
         label: 'Food',
         value: '56'
       }
     );
   });

   test('should not found by value when matching option is missing', () => {
     const option = findPreselectedOption(options, '10');

     expect(option).toBe("");
   });

});
