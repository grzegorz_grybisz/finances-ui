import {
  getCurrentMonth,
  getPreviousMonth,
  getCurrentYear,
  getPreviousYear,
  convertToString,
  parseRange
} from '../util/DateRangeUtils'

describe('For 17th of February 2019', () => {
  const date = new Date(2019, 1, 17);

  describe('start and end date', () => {
    test('of month should be 1st and 28th', () => {

      const dateRange = getCurrentMonth(date);

      expect(dateRange).toEqual({
        startDate: new Date(2019, 1, 1),
        endDate: new Date(2019, 1, 28),
      });
    });

    test('of previous month should be 1st and 31st of January', () => {

      const dateRange = getPreviousMonth(date, 1);

      expect(dateRange).toEqual({
        startDate: new Date(2019, 0, 1),
        endDate: new Date(2019, 0, 31),
      });
    });

    test('of two months back should be 1st and 31st of December 2018', () => {

      const dateRange = getPreviousMonth(date, 2);

      expect(dateRange).toEqual({
        startDate: new Date(2018, 11, 1),
        endDate: new Date(2018, 11, 31),
      });
    });

    test('of year should be 1st of January and 31th of December 2019', () => {

      const dateRange = getCurrentYear(date);

      expect(dateRange).toEqual({
        startDate: new Date(2019, 0, 1),
        endDate: new Date(2019, 11, 31),
      });
    });

    test('of previous year should be 1st of January and 31th of December 2018', () => {

      const dateRange = getPreviousYear(date, 1);

      expect(dateRange).toEqual({
        startDate: new Date(2018, 0, 1),
        endDate: new Date(2018, 11, 31),
      });
    });
  });
});

describe('Should convert', () => {

  test('interval with dates to plain object', () => {
    const range = {
      startDate: new Date(2019, 1, 1),
      endDate: new Date(2019, 1, 28),
    };

    const rangeAsPlainObject = convertToString(range);

    expect(rangeAsPlainObject).toEqual({
      startDate: "2019-02-01",
      endDate: "2019-02-28"
    });
  });

  test('plain object to interval with dates', () => {
    const range = {
      startDate: "2019-02-01",
      endDate: "2019-02-28",
    };

    const parsedRange = parseRange(range);

    expect(parsedRange).toEqual({
      startDate: new Date(2019, 1, 1),
      endDate: new Date(2019, 1, 28)
    });
  });

});
