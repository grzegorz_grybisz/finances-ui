import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';

import ConfigurableMenu from 'Components/Widget/ConfigurableMenu'
import userManager from "../auth/UserManager";

const styles = theme => ({
	avatar: {
		width: 30,
		height: 30
  }
})

class UserAvatar extends React.Component{
	menuConfig = {
		menuItems: [
			{
				key: 'profile',
				label: 'Profile',
				checkbox: false,
				onClick: event => {}
			},
			{
				key: 'logout',
				label: 'Logout',
				checkbox: false,
				onClick: event => this.onLogout(event)
			}
		]
	}

	onLogout = event => {
		event.preventDefault();
		userManager.signoutRedirect();
	}

   render() {
		 return(
			 <ConfigurableMenu config={this.menuConfig}>
			 		<Avatar className={this.props.classes.avatar} src="https://secure.gravatar.com/avatar/8c80dcb76d9b69a5ed4ccfb68d48e94f" />
			 </ConfigurableMenu>
		 );
   }
}

export default withStyles(styles)(UserAvatar);
