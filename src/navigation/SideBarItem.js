import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import ParametrizedLink from '../common/link/ParametrizedLink';

const SideBarItem = props => {
  const { label, icon, url } = props;

  return (
    <ParametrizedLink url={url}>
      <ListItem button>
        <ListItemIcon>
          {icon}
        </ListItemIcon>
        <ListItemText primary={ label } />
      </ListItem>
    </ParametrizedLink>
  )
}

export default SideBarItem;
