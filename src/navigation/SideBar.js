import React from 'react';

import { MuiThemeProvider, makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import TransactionsIcon from '@material-ui/icons/SwapHoriz';
import TemplatesIcon from '@material-ui/icons/OpenInNew';
import SettingsIcon from '@material-ui/icons/Settings';
import TimelineIcon from '@material-ui/icons/ShowChart';
import Divider from '@material-ui/core/Divider';
import Hidden from '@material-ui/core/Hidden';

import SideBarItem from './SideBarItem';
import SideBarFooter from './SideBarFooter';
import AppLogo from './AppLogo';

import { sidebarTheme } from '../themes/themes.js';

const useStyles = makeStyles(theme => ({
	grow: {
		flexGrow: 1,
	},
  divider: {
    margin: theme.spacing(2, 0, 2, 0)
  }
}));

export default function SideBar(props) {
  const classes = useStyles();

  const { sidebarOpen, handleSidebarToggle } = props
  
  const drawer = <div>
    <AppLogo />
      <List dense>
				<SideBarItem label='Timeline' url='/timeline' icon={<TimelineIcon color='primary'/>}/>
        <SideBarItem label='Transactions' url='/transactions' icon={<TransactionsIcon color='primary'/>}/>
        <SideBarItem label='Templates' url='/templates' icon={<TemplatesIcon color='primary'/>}/>
        <Divider className={classes.divider} />
        <SideBarItem label='Settings' url='/settings' icon={<SettingsIcon color='primary'/>}/>
      </List>
      <div className={classes.grow} />
      <SideBarFooter />
  </div>

	return(
  <MuiThemeProvider theme={sidebarTheme}>
    <Hidden smUp>
      <Drawer
        className={props.className}
        classes={{paper: props.className}}
        variant="temporary"
        open={sidebarOpen}
        onClose={handleSidebarToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}>
        {drawer}
      </Drawer>
    </Hidden>
    <Hidden xsDown>
      <Drawer
        className={props.className}
        classes={{paper: props.className}}
        variant="permanent"
        open>
        {drawer}
      </Drawer>
    </Hidden>
  </ MuiThemeProvider>
	);
}
