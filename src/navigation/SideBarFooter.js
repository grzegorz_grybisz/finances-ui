import React from 'react';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { getDeploymentLocation } from '../util/EnvironmentUtils';


const typographyProps = {
  variant: "caption",
  //gutterBottom: true,
}

const SideBarFooter = props => {
  const { text } = props;

  const content = getFooterContent();

  const envInfo = <React.Fragment>
              {content.environment}
              <br />
              {content.mode}
            </React.Fragment>

  return (
    <List dense>
      <ListItem>
        <ListItemText primary={ content.version } secondary={envInfo} primaryTypographyProps={typographyProps}/>
      </ListItem>
    </List>
  )
}

const getFooterContent = () => {
  //TODO move to store

  return {
    version: `UI Version ${VERSION}`,
    mode: `Mode: ${process.env.NODE_ENV}`,
    environment: `Env: ${getDeploymentLocation(DEPLOYED_ON)}`,
  }
}

export default SideBarFooter;
