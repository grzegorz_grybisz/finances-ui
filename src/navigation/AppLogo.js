import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import logo from '../assets/img/app-icon.png';

const styles = theme => ({
	root: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(2),
	},
	logo: {
			height: 40,
			width: 40,
			marginRight: 10
	}
})

const AppLogo = (props) =>(
		<div className={props.classes.root}>
  		<img className={props.classes.logo} src={logo}/>
  		<Typography variant="h6" noWrap className={props.classes.flex}>
  		  FINANCES
			</Typography>
    </ div>
)

export default withStyles(styles)(AppLogo);
