import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import BalanceWidget from 'Components/BalanceWidget/BalanceWidget';
import DateRangePicker from 'Components/DateRangePicker';
import SeachBar from 'Components/SearchBar'
import LogoutTimer from 'Components/LogoutTimer';

import UserAvatar from './UserAvatar.js';

const useStyles = makeStyles(theme => ({
	grow: {
		flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
}));

const TopBar = (props) => {
  const classes = useStyles();
  const {handleSidebarToggle} = props;

  return (
    <AppBar position="fixed" elevation={0} className={props.className}>
      <Toolbar variant="dense">
        <IconButton 
          edge="start" 
          className={classes.menuButton}
          onClick={handleSidebarToggle}>
          <MenuIcon />
        </IconButton>
        <div className={classes.grow} />
				{/* <BalanceWidget/> */}
        <SeachBar />
				<div className={classes.grow} />
				<DateRangePicker/>
        <div className={classes.grow} />
				<LogoutTimer />
        <UserAvatar />
      </Toolbar>
    </AppBar>
  )
}

export default TopBar;