import {
  REQUEST_TEMPLATES,
  RECIEVE_TEMPLATES
} from './action.js'

import { convertTagsToSelectOptions } from '../../util/DtoUtils'

const defaultState = {
  isFetching: false,
  items: []
};

export function templates(state = defaultState, action){
  switch (action.type){
    case REQUEST_TEMPLATES:
      return {
        ...state,
        isFetching: true,
      }
    case RECIEVE_TEMPLATES:
      return {
        ...state,
        isFetching: false,
        items: action.templates,
      }
    default:
      return state
  }
}
