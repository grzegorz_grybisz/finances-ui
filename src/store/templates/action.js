import fetch from 'cross-fetch'
import { store } from "../store";

import { notifySuccess } from '../snackbars/action'
import { notifyError } from '../snackbars/action'

export const ADDED_TEMPLATE = 'ADDED_TEMPLATE';
export const POST_TEMPLATE = 'POST_TEMPLATE';

export function addTemplate(templateDto){
  return function(dispatch){
    dispatch(postTemplate())

    const token = store.getState().oidc.user.access_token;
    var url = new URL(TEMPLATES_HOST + '/finances/templates');

    return fetch(url,
      {
        method: "POST",
        body: JSON.stringify(templateDto),
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(
        handleRequestErrors,
        error => console.log('Error when posting template', error)
      )
      .then(response => {
        dispatch(notifySuccess("New Template added!"))
        dispatch(successfulyAddedTemplate(templateDto))
        dispatch(fetchTemplates())
      })
      .catch(error => {
        console.log(error)
        dispatch(notifyError('Error when adding transaction: '+ error.message))
      })
  }
}

export function successfulyAddedTemplate(templateDto){
  return {
    type: ADDED_TEMPLATE,
    templateDto
  }
}

export function postTemplate(){
  return {
    type: POST_TEMPLATE
  }
}


export const REQUEST_TEMPLATES = 'REQUEST_TEMPLATES';
export const RECIEVE_TEMPLATES = 'RECIEVE_TEMPLATES';

export function fetchTemplates(){
  return function(dispatch){
      dispatch(requestTemplates())

      const token = store.getState().oidc.user.access_token;
      var url = new URL(TEMPLATES_HOST + '/finances/templates');

      return fetch(url,
        {
          mode: 'cors',
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(
          response => response.json(),
          error => console.log('Error when getting templates', error)
        )
        .then(json => dispatch(recieveTemplates(json)))
    }
}

function requestTemplates(){
  return {
    type: REQUEST_TEMPLATES
  }
}

function recieveTemplates(json){
  return {
    type: RECIEVE_TEMPLATES,
    templates: json,
    recievedAt: Date.now()
  }
}

const handleRequestErrors = response => {
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
    return response;
}
