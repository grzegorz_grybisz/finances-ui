import {
  REQUEST_TAGS,
  RECIEVE_TAGS,
} from './action.js'

export function tags(
  state = {
    isFetching: false,
    items: []
  },
  action
){
  switch (action.type){
    case REQUEST_TAGS:
      return {
        ...state,
        isFetching: true,
      }
    case RECIEVE_TAGS:
      return {
        ...state,
        isFetching: false,
        items: action.tags,
      }
    default:
      return state
  }
}
