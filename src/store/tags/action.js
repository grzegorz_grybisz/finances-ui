import fetch from 'cross-fetch'
import { store } from "../store";

export const REQUEST_TAGS = 'REQUEST_TAGS';
function requestTags(){
  return {
    type: REQUEST_TAGS
  }
}

export const RECIEVE_TAGS = 'RECIEVE_TAGS';
function recieveTags(json){
  return {
    type: RECIEVE_TAGS,
    tags: json,
    recievedAt: Date.now()
  }
}

export function fetchTags(){
  return function(dispatch){
      dispatch(requestTags())

      const token = store.getState().oidc.user.access_token;
      var url = new URL(FINANCES_HOST + '/finances/transaction/tag');

      return fetch(url,
        {
          mode: 'cors',
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(
          response => response.json(),
          error => console.log('Error when getting tags', error)
        )
        .then(json => dispatch(recieveTags(json)))
    }
}
