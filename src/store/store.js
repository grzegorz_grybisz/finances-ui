import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware} from 'redux'
import { createUserManager, loadUser } from 'redux-oidc';
import { createLogger  } from 'redux-logger';

import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import financesReducers from './reducers'

import userManager from '../auth/UserManager'


const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['filter']
}

const persistedReducer = persistReducer(persistConfig, financesReducers);

const loggerMiddleware = createLogger({
  collapsed: true,
  duration: true
});

const store = createStore(
  persistedReducer,
  applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
  )
)

loadUser(store, userManager);

const persistor = persistStore(store);

export { store, persistor }
