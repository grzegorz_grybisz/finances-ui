import fetch from 'cross-fetch'
import { store } from "../store";

export const REQUEST_CATEGORIES = 'REQUEST_CATEGORIES';
export const RECIEVE_CATEGORIES = 'RECIEVE_CATEGORIES';

function requestCategories(){
  return {
    type: REQUEST_CATEGORIES
  }
}

function recieveCategories(json){
  return {
    type: RECIEVE_CATEGORIES,
    categories: json,
    recievedAt: Date.now()
  }
}

export function fetchCategories(){
    return function(dispatch){
      dispatch(requestCategories());

      var token = "";
      if(store.getState().oidc.user){
        token = store.getState().oidc.user.access_token;
      }

      const url = FINANCES_HOST + '/finances/transaction/category';

      return fetch(url,
        {
          mode: 'cors',
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(
          response => response.json(),
          error => console.log('Error when getting transactions', error)
        )
        .then(json => dispatch(recieveCategories(json)))
    }
}
