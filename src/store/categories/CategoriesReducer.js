import {
  REQUEST_CATEGORIES,
  RECIEVE_CATEGORIES
} from './CategoriesActions.js'

function categories(
  state = {
    isFetching: false,
    items: []
  },
  action
){
  switch (action.type){
    case RECIEVE_CATEGORIES:
//      console.log(action);
      return {
        isFetching: false,
        items: action.categories
      }
    default:
      return state
  }
}

export default categories;
