export const CHANGE_SEARCH_TEXT = 'CHANGE_SEARCH_TEXT';

export function changeSearchText(newValue) {
    return {
        type: CHANGE_SEARCH_TEXT,
        newValue
    }
}  