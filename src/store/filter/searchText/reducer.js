import { CHANGE_SEARCH_TEXT } from './action'

export function searchText(state = "", action){
    switch (action.type){
      case CHANGE_SEARCH_TEXT:
        return action.newValue;
      default:
        return state;
    }
  }