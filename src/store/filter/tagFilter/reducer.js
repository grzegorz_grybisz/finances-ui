import {
    TOGGLE_FILTER_TAG,
} from './action.js'

const defaultState = {
    searchMode: 'ALL',
    items: []
};

const toggleFilterTag = (state, action) => {
    const items = toggleTagInArray(state.items, action.tag);
    const searchMode = computeMode(items);

    return {
        ...state,
        searchMode,
        items,
    }
}

const toggleTagInArray = (items, tag) => {
    if(items.indexOf(tag) == -1){
        return [
            ...items,
            tag,
        ]
    }
    return items.filter(item => item != tag);
}

const computeMode = items => {
    if(items.length == 0) {
        return 'ALL';
    }
    return 'SELECTED';
}

export function tagFilter(state = defaultState, action) {
    switch (action.type) {
        case TOGGLE_FILTER_TAG:
           return toggleFilterTag(state, action);
        default:
            return state;
    }
}
