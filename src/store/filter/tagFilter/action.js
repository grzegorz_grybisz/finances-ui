export const TOGGLE_FILTER_TAG = 'TOGGLE_FILTER_TAG';

export function toggleFilterTag(tag) {
    return {
        type: TOGGLE_FILTER_TAG,
        tag
    }
}