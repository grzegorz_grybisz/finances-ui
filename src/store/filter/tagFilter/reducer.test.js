import { tagFilter as reducer } from './reducer';
import { toggleFilterTag } from './action';

describe('Tag Filter reducer', () => {
    it('should return initial state', () => {

        const initialState = reducer(undefined, {});

        expect(initialState).toEqual({
            searchMode: 'ALL',
            items: []
        });
    });

    describe('should handle adding tags to', () => {
        it('empty filter', () => {

            const action = toggleFilterTag('testTag');
            const state = reducer(undefined, action);

            expect(state).toEqual({
                searchMode: 'SELECTED',
                items: ['testTag']
            });
        });

        it('not empty filter', () => {

            const originalState = {
                searchMode: 'SELECTED',
                items: ['testTag']
            }

            const action = toggleFilterTag('newTag');
            const state = reducer(originalState, action);

            expect(state).toEqual({
                searchMode: 'SELECTED',
                items: ['testTag', 'newTag']
            });
        });
    });

    describe('should handle removing', () => {

        it('one tag from filter', () => {

            const originalState = {
                searchMode: 'SELECTED',
                items: ['testTag', 'anotherTag']
            }

            const action = toggleFilterTag('testTag');
            const state = reducer(originalState, action);

            expect(state).toEqual({
                searchMode: 'SELECTED',
                items: ['anotherTag']
            });
        })

        it('last tag from filter', () => {

            const originalState = {
                searchMode: 'SELECTED',
                items: ['testTag']
            }

            const action = toggleFilterTag('testTag');
            const state = reducer(originalState, action);

            expect(state).toEqual({
                searchMode: 'ALL',
                items: []
            });
        })
    });
}
);