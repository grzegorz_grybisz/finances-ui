import {
  CHANGE_DATE_INTERVAL,
} from './action.js'

import {
  getCurrentMonth,
  convertToString
} from '../../../util/DateRangeUtils'

const defaultState = convertToString(getCurrentMonth(new Date()));

export function dateFilter(state = defaultState, action){
  switch (action.type){
    case CHANGE_DATE_INTERVAL:
      return {
        ...state,
        ...convertToString(action.newInterval)
      }
    default:
      return state;
  }
}
