export const CHANGE_DATE_INTERVAL = 'CHANGE_DATE_INTERVAL';

export function changeDateInterval(newInterval){
  return {
    type: CHANGE_DATE_INTERVAL,
    newInterval
  }
}
