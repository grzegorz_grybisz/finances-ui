import { combineReducers } from 'redux';

import { dateFilter } from './dateFilter/reducer'
import { tagFilter } from './tagFilter/reducer';
import { searchText } from './searchText/reducer';

export const filter = combineReducers({
  searchText,
  dateFilter,
  tagFilter,
});
