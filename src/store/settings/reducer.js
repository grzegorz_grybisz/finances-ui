import {
  CHANGE_SETTING,
} from './action.js'

const defaultState = {
  isDense: true,
  hasItemDividers: false,
  hasThickDividers: false,
};

export function settings(state = defaultState, action){
  switch (action.type){
    case CHANGE_SETTING:
      return {
        ...state,
        ...action.settings
      }
    default:
      return state
  }
}
