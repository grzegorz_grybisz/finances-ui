export const CHANGE_SETTING = "CHANGE_SETTING";

export const changeSetting = (setting, value) => {

  return {
    type: CHANGE_SETTING,
    settings: {
      [setting]: value
    },
  }
};
