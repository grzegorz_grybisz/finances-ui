import {
  REQUEST_WALLETS,
  RECIEVE_WALLETS,
} from './action.js'

export function wallets(
  state = {
    isFetching: false,
    items: []
  },
  action
){
  switch (action.type){
    case REQUEST_WALLETS:
      return {
        ...state,
        isFetching: true,
      }
    case RECIEVE_WALLETS:
      return {
        ...state,
        isFetching: false,
        items: action.wallets,
        defaultWallet: action.wallets[0]
      }
    default:
      return state
  }
}
