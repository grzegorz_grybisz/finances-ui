import fetch from 'cross-fetch'
import { store } from "../store";

export const REQUEST_WALLETS = 'REQUEST_WALLETS';
function requestWallets(){
  return {
    type: REQUEST_WALLETS
  }
}

export const RECIEVE_WALLETS = 'RECIEVE_WALLETS';
function recieveWallets(json){
  return {
    type: RECIEVE_WALLETS,
    wallets: json,
    recievedAt: Date.now()
  }
}

export function fetchWallets(){
  return function(dispatch){
      dispatch(requestWallets())

      const token = store.getState().oidc.user.access_token;
      var url = new URL(FINANCES_HOST + '/finances/transaction/wallet');

      return fetch(url,
        {
          mode: 'cors',
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(
          response => response.json(),
          error => console.log('Error when getting transactions', error)
        )
        .then(json => dispatch(recieveWallets(json)))
    }
}
