import { combineReducers } from 'redux';
import { reducer as oidc } from 'redux-oidc';

import categories from './categories/CategoriesReducer';
import { transactionSearchText, transactions } from './transactions/reducer';
import { templates } from './templates/reducer';
import { wallets } from './wallets/reducer';
import { tags } from './tags/reducer';
import { snackbars } from './snackbars/reducer';
import { settings } from './settings/reducer';
import { filter } from './filter/reducer';

const financesReducers = combineReducers({
  oidc,
  transactions,
  wallets,
  categories,
  tags,
  templates,
  snackbars,
  settings,
  filter,
})

export default financesReducers;
