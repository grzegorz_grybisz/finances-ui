
export const getFilterTags = state => {
    if (state.filter.tagFilter.searchMode == 'ALL') {
        return state.tags.items;
    }
    return state.filter.tagFilter.items;
}

export const getSearchText = state => {
    return state.filter.searchText;
}

export const getFilterDateRange = state => {
    return state.filter.dateFilter;
}

export const getFilteredTransactions = state => {
    return getMatchingItems(
        state.transactions.items,
        getSearchText(state)
    )
}

export const getFilteredTemplates = state => {
    return getMatchingItems(
        state.templates.items,
        getSearchText(state)
    )
}

const getMatchingItems = (items, searchText) => {
    return items.filter((item) => {
        return containsSearchText(item.name, searchText);
    });
}

const containsSearchText = (text, searchText) => {
    const lowerCaseName = text.toLowerCase();
    const lowerCaseSearchText = searchText.toLowerCase();
    return lowerCaseName.includes(lowerCaseSearchText);
}