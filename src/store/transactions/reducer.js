import {
  ADDED_TRANSACTION,
  REQUEST_TRANSACTIONS,
  RECIEVE_TRANSACTIONS,
  REQUEST_SINGLE_TRANSACTION,
  RECIEVE_SINGLE_TRANSACTION
} from './action.js'

import { convertDateToString } from '../../util/DateUtils'

const defaultState = {
  isFetching: false,
  items: [],
  editedItem: {},
  lastCreatedOn: convertDateToString(new Date()),
};

export function transactions(state = defaultState, action){
  switch (action.type){
    case ADDED_TRANSACTION:
      return {
        ...state,
        lastCreatedOn: action.newTransaction.date,
      }
    case REQUEST_TRANSACTIONS:
      return {
        ...state,
        isFetching: true,
      }
    case RECIEVE_TRANSACTIONS:
      return {
        ...state,
        isFetching: false,
        items: action.transactions,
      }
    case RECIEVE_SINGLE_TRANSACTION:
      return {
        ...state,
        isFetching: false,
        items: [
        ...state.items
        ]
      }
    default:
      return state
  }
}
