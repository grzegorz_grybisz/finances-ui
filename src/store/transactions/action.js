import fetch from 'cross-fetch'
import { store } from "../store";
import { getFilterDateRange } from "../selectors";

import { notifySuccess } from '../snackbars/action'
import { notifyError } from '../snackbars/action'

export const ADDED_TRANSACTION = 'ADDED_TRANSACTION';
export const POST_TRANSACTION = 'POST_TRANSACTION';

export function addTransaction(newTransaction){
  return function(dispatch){
    dispatch(postTransaction())

      const token = store.getState().oidc.user.access_token;
      const url = FINANCES_HOST + '/finances/transaction';

    return fetch(url,
      {
        method: "POST",
        body: JSON.stringify(newTransaction),
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      })
      .then(
        handleRequestErrors,
        error => console.log('Error when posting transactions', error)
      )
      .then(response => {
        dispatch(notifySuccess("New Transaction added!"))
        dispatch(successfulyAddedTransaction(newTransaction))
        dispatch(fetchTransactions())
      })
      .catch(error => {
        console.log(error)
        dispatch(notifyError('Error when adding transaction: '+ error.message))
      })
  }
}

export function successfulyAddedTransaction(newTransaction){
  return {
    type: ADDED_TRANSACTION,
    newTransaction
  }
}

export function postTransaction(){
  return {
    type: POST_TRANSACTION
  }
}

export const REST_DELETE_TRANSACTION = 'REST_DELETE_TRANSACTION';
export function restDeleteTransaction(){
  return {
    type: REST_DELETE_TRANSACTION
  }
}

export function deleteTransaction(id){
  return function(dispatch){

     dispatch(restDeleteTransaction())

     const token = store.getState().oidc.user.access_token;
     const url = FINANCES_HOST + `/finances/transaction/${id}`;

     return fetch(url,
       {
         method: "DELETE",
         mode: 'cors',
         headers: {
            'Authorization': 'Bearer ' + token
          }
       })
       .then(
         handleRequestErrors,
         error => console.log('Error when deleting transactions', error)
       )
       .then(response => {
         dispatch(notifySuccess("Transaction removed!"))
         dispatch(fetchTransactions())
       })
       .catch(error => {
         console.log(error)
         dispatch(notifyError('Error when removing transaction: '+ error.message))
       })
   }
}

export const UPDATE_TRANSACTION = 'UPDATE_TRANSACTION';
export const PUT_TRANSACTION = 'PUT_TRANSACTION';
export const RECIEVE_UPDATE_TRANSACTION_RESPONSE = 'RECIEVE_UPDATE_TRANSACTION_RESPONSE';

export function putTransaction(){
  return {
    type: PUT_TRANSACTION
  }
}

export function updateTransaction(id, updatedTransaction){
  return function(dispatch){

     dispatch(putTransaction())


    const token = store.getState().oidc.user.access_token;
     const url = FINANCES_HOST + `/finances/transaction/${id}`;

     return fetch(url,
       {
         method: "PUT",
         body: JSON.stringify(updatedTransaction),
         mode: 'cors',
         headers: {
           'Content-Type': 'application/json',
           'Accept': 'application/json',
           'Authorization': 'Bearer ' + token
         }
       })
       .then(
         handleRequestErrors,
         error => console.log('Error when updating transactions', error)
       )
       .then(response => {
         dispatch(notifySuccess("'Transaction updated!'"))
         dispatch(fetchTransactions())
       })
       .catch(error => {
         console.log(error)
         dispatch(notifyError('Error when updating transaction: '+ error.message))
       })
   }
}

export const REQUEST_TRANSACTIONS = 'REQUEST_TRANSACTIONS';
export const RECIEVE_TRANSACTIONS = 'RECIEVE_TRANSACTIONS';

export function fetchTransactions(){
  return function(dispatch){
      dispatch(requestTransactions())

      const token = store.getState().oidc.user.access_token;
      var url = new URL(FINANCES_HOST + '/finances/transaction');

      const params = getFilterDateRange(store.getState());

      Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

      return fetch(url,
        {
          mode: 'cors',
          headers: {
            'Authorization': 'Bearer ' + token
          }
        })
        .then(
          response => response.json(),
          error => console.log('Error when getting transactions', error)
        )
        .then(json => dispatch(recieveTransactions(json)))
    }
}

function requestTransactions(){
  return {
    type: REQUEST_TRANSACTIONS
  }
}

function recieveTransactions(json){
  return {
    type: RECIEVE_TRANSACTIONS,
    transactions: json,
    recievedAt: Date.now()
  }
}

export const REQUEST_SINGLE_TRANSACTION = 'REQUEST_SINGLE_TRANSACTION';
export const RECIEVE_SINGLE_TRANSACTION = 'RECIEVE_SINGLE_TRANSACTION';

export function fetchTransaction(id){
  return function(dispatch){
    dispatch(requestSingleTransaction())

    const json = {};

    dispatch(recieveTransactions(json));
  }
}

function requestSingleTransaction(){
  return {
    type: REQUEST_SINGLE_TRANSACTION
  }
}

function recieveSingleTransaction(json){
  return {
    type: RECIEVE_SINGLE_TRANSACTIONS,
    transaction: json,
    recievedAt: Date.now()
  }
}

const handleRequestErrors = response => {
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
    return response;
}
