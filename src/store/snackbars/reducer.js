import {
  ENQUEUE_SNACKBAR,
  REMOVE_SNACKBAR
} from './action.js'

const defaultState = {
    notifications: [],
};

const defaultOptions = {
  variant: 'info',
  autoHideDuration: 3000,
}

export function snackbars (state = defaultState, action) {
    switch (action.type) {
        case ENQUEUE_SNACKBAR:
            const newNotification = mergeNotificationWithDefaultOptions(action.notification);

            return {
                ...state,
                notifications: [
                    ...state.notifications,
                    {
                        ...newNotification,
                    },
                ],
            };

        case REMOVE_SNACKBAR:
            return {
                ...state,
                notifications: state.notifications.filter(
                    notification => notification.key !== action.key,
                ),
            };

        default:
            return state;
    }
};

const mergeNotificationWithDefaultOptions = notification => {
  const { options, ...rest } = notification;

  return {
    ...rest,
    options: { ...defaultOptions, ...options}
  }
}
