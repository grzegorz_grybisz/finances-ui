export const ENQUEUE_SNACKBAR = "ENQUEUE_SNACKBAR";
export const REMOVE_SNACKBAR = "REMOVE_SNACKBAR";

export const notifySuccess = message => {

  const snackbar = {
    message: message,
    options: {
      variant: 'success'
    }
  }

  return enqueueSnackbar(snackbar);
};

export const notifyError = message => {

  const snackbar = {
    message: message,
    options: {
      variant: 'error',
      autoHideDuration: null,
    }
  }

  return enqueueSnackbar(snackbar);
};

export const enqueueSnackbar = notification => {
  return {
    type: ENQUEUE_SNACKBAR,
    notification: {
        key: new Date().getTime() + Math.random(),
        ...notification,
    },
}};

export const removeSnackbar = key => {
  return {
    type: REMOVE_SNACKBAR,
    key,
}};
