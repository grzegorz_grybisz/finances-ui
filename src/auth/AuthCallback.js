import React from "react";
import { connect } from "react-redux";
import { CallbackComponent } from "redux-oidc";
import SplashPage from '../SplashPage'


import userManager from "./UserManager";

class AuthCallback extends React.Component {

  onSuccessCallback = () => {
    this.props.history.push('/');
    console.log("Successful callback");
  }

  onError = error => {
    this.props.history.push('/login');
    console.log("Error during authentication");
    console.error(error);
  }

  render() {
    return (
      <CallbackComponent
        userManager={userManager}
        successCallback={this.onSuccessCallback}
        errorCallback={this.onError}
        >
        <SplashPage />
      </CallbackComponent>
    );
  }
}

export default connect()(AuthCallback);
