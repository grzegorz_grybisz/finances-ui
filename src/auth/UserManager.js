import { createUserManager } from 'redux-oidc';

const userManagerConfig = {
  authority: 'https://finances-keycloak.herokuapp.com/auth/realms/Finances-App',
  client_id: 'login-app',
  redirect_uri: `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ''}/callback`,
  silent_redirect_uri: `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ''}/silentcallback`,
  post_logout_redirect_uri:  `${window.location.protocol}//${window.location.hostname}${window.location.port ? `:${window.location.port}` : ''}/callback`,
  response_type: 'id_token token',
  scope: 'openid',
  loadUserInfo: true,
};

const userManager = createUserManager(userManagerConfig);

export default userManager;
