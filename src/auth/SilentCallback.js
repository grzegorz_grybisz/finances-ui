import React from "react";
import { connect } from "react-redux";
import { CallbackComponent } from "redux-oidc";
import SplashPage from '../SplashPage'


import userManager from "./UserManager";

class SilentCallback extends React.Component {

  componentDidMount() {
    userManager.signinSilentCallback()
      .then((user) => console.log("Silent Signin callback success"))
      .catch((error) => console.log("Silent Signing callback error: ", error));
  }

  render() {
     return (<SplashPage />)
  }
}

export default connect()(SilentCallback);
