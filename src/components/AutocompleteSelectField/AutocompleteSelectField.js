import React from 'react';
import Select from 'react-select';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CancelIcon from '@material-ui/icons/Cancel';
import { withStyles } from '@material-ui/core/styles';

import TagChip from 'Components/TagChip/TagChip';

const styles = theme => ({
  input: {
    display: 'flex',
    padding: 0,
    height: "auto",
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden',
  },
  noOptionsMessage: {
    padding:  theme.spacing(1, 2),
  },
  singleValue: {
    fontSize: 16,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  divider: {
    height: theme.spacing(2),
  },
});

class AutocompleteSelectField extends React.Component{

   handleChange = selectedOption => {
     const fieldId = this.props.fieldConfig.id;
     this.props.setFieldValue(fieldId, selectedOption);
   }

   render() {
     const {
       fieldConfig,
       classes,
       value,
       setFieldValue,
       isSimpleSelect
      } = this.props;

     const innerValue = prepareValueForInnerTextField(value);

     return (
       <Select
         id={fieldConfig.id}
         classes={classes}
         value={value}
         onChange={this.handleChange}
         options={fieldConfig.selectItems}
         openMenuOnClick={isSimpleSelect}
         isSearchable={!isSimpleSelect}
         components={components}
         menuPortalTarget={document.body}
         menuPlacement='auto'
         styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
         textFieldProps={{
            label: fieldConfig.label,
            value: innerValue
          }}
         placeholder={null}
         isMulti={this.props.isMulti}
       />
     );
   }
}

const prepareValueForInnerTextField = value => {
  if(value && value.length){
    return value[0].label;
  }
  if(value && value.label){
    return value.label;
  }
  return "";
}

const components = {
  Control,
  Placeholder,
  Menu,
  Option,
  SingleValue,
  MultiValue,
  NoOptionsMessage,
  ValueContainer
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

function Control(props) {
  return(
    <TextField
      fullWidth
      margin="none"
      InputProps={{
        inputComponent,
        inputProps: {
          className: props.selectProps.classes.input,
          inputRef: props.innerRef,
          children: props.children,
          ...props.innerProps,
        },
      }}
      {...props.selectProps.textFieldProps}
    />
  )
}

function Placeholder(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.placeholder}
      {...props.innerProps}
      >
      {props.children}
    </Typography>
  );
}

function Menu(props) {
  return (
    <Paper
      className={props.selectProps.classes.paper}
      square {...props.innerProps}>
      {props.children}
    </Paper>
  );
}

function ValueContainer(props) {
  return <div className={props.selectProps.classes.valueContainer}>{props.children}</div>;
}

function MultiValue(props) {
  return (
    <TagChip
      label={props.children}
      onDelete={props.removeProps.onClick}
    />
  );
}

function SingleValue(props) {
  return (
    <Typography
      className={props.selectProps.classes.singleValue}
      {...props.innerProps}
      >
      {props.children}
    </Typography>
  );
}

function NoOptionsMessage(props) {
  return (
    <Typography
      color="textSecondary"
      className={props.selectProps.classes.noOptionsMessage}
      {...props.innerProps}
    >
      {props.children}
    </Typography>
  );
}

function Option(props) {
  return (
    <MenuItem
      buttonRef={props.innerRef}
      selected={props.isFocused}
      component="div"
      {...props.innerProps}
    >
      {props.children}
    </MenuItem>
  );
}

function Group(props){
  return(
  <div style={groupStyles}>
    <components.Group {...props}/>
  </div>
);
}


export default withStyles(styles)(AutocompleteSelectField);
