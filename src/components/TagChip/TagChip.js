import React from 'react';

import { makeStyles } from '@material-ui/core/styles'
import Chip from '@material-ui/core/Chip';
import DoneIcon from '@material-ui/icons/Done';
import AddIcon from '@material-ui/icons/Add';

const useStyles = makeStyles(theme => ({
  chip: {
    margin:  theme.spacing(0.25, 0.25)
  },
}));

export default function TagChip (props) {

  const classes = useStyles();
  const {label, onDelete, onClick, isSelected} = props;

  const icon = isSelected ? <DoneIcon /> : <AddIcon/>;
  const variant = isSelected ? 'outlined' : 'default';
  const color = isSelected ? 'primary' : 'default';

  return <Chip
            size="small"
            className={classes.chip}
            icon={icon}
            label={label}
            onDelete={onDelete}
            onClick={onClick}
            color={color}
            clickable
           // variant={variant}
          />
}