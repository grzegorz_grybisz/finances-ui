import React from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const CheckboxField = props =>{
  const {fieldConfig} = props;

  return (
    <FormControlLabel
      control={
          <Checkbox checked={fieldConfig.dynamicValue()} onChange={fieldConfig.onClick}/>
        }
      label={fieldConfig.label}
    />
  );
}

export default CheckboxField;
