import React from 'react';
import { connect } from 'react-redux'


import { Chart, Title, ArgumentAxis, BarSeries } from "@devexpress/dx-react-chart-material-ui";
import { Stack, Animation } from '@devexpress/dx-react-chart';

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import { changeTransactionSearchText, fetchTransactions } from '../../store/transactions/action'
import { groupByKey, groupBy } from '../../util/ArrayUtils'

const data = [
  {account: "base", incomes: 1000, expenses: 100}
];

const BalanceWidget = (props) => {
  return (
    <div>
        <Chart
          data={data}
          width={300}
          height={50}
          rotated>
            <BarSeries
              valueField="incomes"
              argumentField="account"
              color="rgb(89, 190, 59)"
              barWidth={1}/>
            <BarSeries
              valueField="expenses"
              argumentField="account"
              color="rgb(222, 42, 42)"
              barWidth={1}/>
            <Stack />
        </Chart>
      </div>
  );
}

const mapTransactionsToChartData = (transactions) => {

  const groups = Object.entries(groupBy(transactions, 'date'));
  console.log(groups);
  //const data = groups.map(([key, v]) => { return {argument: key, value: v}});
  const data = groups.map(mapTransactionGroupToChartValue);
  console.log(data);
  return data;
}

const mapTransactionGroupToChartValue = ([date, v]) => {
  const reducer = (accumulator, currentValue) => accumulator + currentValue;
  const sumOfAmounts = v.map(i => i.amount).reduce(reducer);

  return {
    argument: date,
    value: sumOfAmounts
  }
}


//TODO use reselect library (later ;) )
const mapStateToProps = state =>{
  return {
  //  data: mapTransactionsToChartData(state.transactions.items),
  //  isFetching: state.transactions.isFetching,
  }
}

const mapDispatchToProps = dispatch => {
    dispatch(fetchTransactions());
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(BalanceWidget);
