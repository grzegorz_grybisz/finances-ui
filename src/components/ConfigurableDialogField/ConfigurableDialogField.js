import React from 'react';

import ConfigurableTextField from 'Components/ConfigurableTextField/ConfigurableTextField';
import AmountField from 'Components/AmountField/AmountField';
import AutocompleteSelectField from 'Components/AutocompleteSelectField/AutocompleteSelectField';
import ConfigurableDatePicker from 'Components/ConfigurableDatePicker/ConfigurableDatePicker';

const ConfigurableDialogField = (props) =>{
  const fieldConfig = props.fieldConfig;

  if(fieldConfig.autocomplete){
    return <AutocompleteSelectField {...props} />;
  }
  if(fieldConfig.multiselect){
    return <AutocompleteSelectField {...props} isMulti/>;
  }
  if(fieldConfig.select){
    return <AutocompleteSelectField {...props} isSimpleSelect/>;
  }
  if(fieldConfig.date){
    return <ConfigurableDatePicker {...props} />;
  }
  if(fieldConfig.amount){
    return <AmountField {...props} />;
  }
  return <ConfigurableTextField {...props}/>;
}

export default ConfigurableDialogField;
