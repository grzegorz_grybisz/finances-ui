import React from 'react';

import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  medium: {
    fontSize: '1em'
  },
  large: {
    fontSize: '1.5em'
  },
});

const ConfigurableTextField = (props) => {
  const {
    fieldConfig,
    value,
    classes,
    endAdornment,
    type,
  } = props;

  const inputClass = getClass(classes, fieldConfig.size);
  const adornments = getAdornments(inputClass, endAdornment);

  return (
    <TextField
      id={fieldConfig.id}
      label={fieldConfig.label}
      autoFocus={fieldConfig.autoFocus}
      margin="none"
      onChange={props.onChange}
      value={value}
      fullWidth
      type={type}
      InputProps={{
        classes: { input: inputClass },
        ...adornments,
      }}
    />
  );
}

const getClass = (classes, size) => {
  if(!size){
    return classes.medium;
  }
  return classes[size];
}

const getAdornments = (inputClass, endAdornment) => {
  if(endAdornment){
    return {
      endAdornment: <ResizableAdornment position="start" typographyClass={inputClass}>{endAdornment}</ResizableAdornment>,
    }
  }

  return {};
}

const ResizableAdornment = (props) => {
  const {position, typographyClass, children} = props;

  return (
    <InputAdornment position={position}>
      <Typography color='textSecondary' className={typographyClass}>
        {children}
      </Typography>
    </InputAdornment>
  );
}

export default withStyles(styles)(ConfigurableTextField);
