import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getSearchText } from '../../store/selectors';
import { changeSearchText } from '../../store/filter/searchText/action'

import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

const SearchBar = props => {
  const searchText = useSelector(getSearchText);
  const dispatch = useDispatch()

  const onTextChange = e => dispatch(changeSearchText(e.target.value));

  return (
    <Input
      placeholder="Search..."
      type="search"
      value={searchText}
      onChange={onTextChange}
      startAdornment={
        <InputAdornment position="start">
          <SearchIcon color='primary'/>
        </InputAdornment>
      }/>
  )
}

export default SearchBar;
