import React, { Fragment } from 'react';

import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Grid from '@material-ui/core/Grid';

import ActionButton from 'Components/ActionButton/ActionButton'

const ActionsPanel = (props) =>{
  const {buttons} = props.config;

  const actionButtons = generateActionButtons(buttons);
  return (<ListItemSecondaryAction>
            <Fragment>
              {actionButtons}
            </Fragment>
          </ListItemSecondaryAction>);
}

const generateActionButtons = (buttons) => {
  return buttons.map(generateActionButton);
}

const generateActionButton = (config) => {
   const key = config.label;

 	return (
    <ActionButton key={key} config = {config}/>
  );
}


export default ActionsPanel;
