import { EXPENSE, INCOME } from '../../util/transactionUtils'

export const getDisplayedColor = (transactionType) =>{
  if(transactionType == INCOME){
    return 'green';
  } if(transactionType == EXPENSE){
    return 'red';
  }
}

export const getDisplayedAmount = (amount, transactionType) =>{
  const signedAmount = determineSign(amount, transactionType)
  return Number(signedAmount).toFixed(2).replace(/\./g, ',');;
//  return numberFormatter.format(signedAmount);
}

const determineSign = (amount, transactionType) => {
  if(transactionType == INCOME){
    return amount;
  }else if(transactionType == EXPENSE){
    return -amount;
  }
}

var numberFormatter = new Intl.NumberFormat('pl-PL', { useGrouping: false, minimumFractionDigits: 2, maximumFractionDigits: 2 });
