import { EXPENSE, INCOME } from '../../util/transactionUtils'
import { getDisplayedColor, getDisplayedAmount } from './moneyAmountText'


describe('money amount text', () => {
  describe('color', () => {
    test('is red when transaction is an expense', () => {
      const color = getDisplayedColor(EXPENSE);
      expect(color).toBe("red");
    });

    test('is green when transaction is an income', () => {
      const color = getDisplayedColor(INCOME);
      expect(color).toBe("green");
    })
  });

  describe('value sign', () => {
    test('is minus when transaction is an expense', () => {
      const displayedAmount = getDisplayedAmount(10, EXPENSE);
      expect(displayedAmount).toBe('-10,00');
    });

    test('is plus when transaction is an income', () => {
      const displayedAmount = getDisplayedAmount(10, INCOME);
      expect(displayedAmount).toBe('10,00');
    })
  });

  describe('value has two decimal places', () => {
    test('when given value is integer', () => {
      const displayedAmount = getDisplayedAmount(10, INCOME);
      expect(displayedAmount).toBe('10,00');
    });

    test('when given value has one decimal place', () => {
      const displayedAmount = getDisplayedAmount(10.1, INCOME);
      expect(displayedAmount).toBe('10,10');
    })

    test('when given value has two decimal places', () => {
      const displayedAmount = getDisplayedAmount(10.12, INCOME);
      expect(displayedAmount).toBe('10,12');
    })

    test('when given value has three decimal places', () => {
      const displayedAmount = getDisplayedAmount(10.123, INCOME);
      expect(displayedAmount).toBe('10,12');
    })
  });
});
