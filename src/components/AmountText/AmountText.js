import React from 'react';
import { getDisplayedColor, getDisplayedAmount } from './moneyAmountText'

const AmountText = (props) =>{
  const { text, type } = props;

  var textStyle = {
    color: getDisplayedColor(type),
    textAlign: 'right'
  };

  if(text){
    return (
      <p style={textStyle}>{getDisplayedAmount(text, type)} PLN</p>
    );
  }
  return <p style={textStyle}>No amount</p>;
}

export default AmountText;
