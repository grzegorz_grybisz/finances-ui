import React from 'react';
import { connect } from 'react-redux'

import { withStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';

import DateRangePickerPanel from './DateRangePickerPanel';
import DateRangeInput from './DateRangeInput';

import { getFilterDateRange } from '../../store/selectors';
import { changeDateInterval } from '../../store/filter/action'
import { fetchTransactions } from '../../store/transactions/action'
import { parseRange } from '../../util/DateRangeUtils'

const styles = theme => ({
  popoverContent: {
    margin: theme.spacing(2),
  },
});

class DateRangePicker extends React.Component {
  state = {
     anchorEl: null,
     startDate: this.props.dateRange.startDate,
     endDate: this.props.dateRange.endDate,
  };

  handleStartDateChange = date => {
    this.setState({ startDate: date });
  };

  handleEndDateChange = date => {
    this.setState({ endDate: date });
  };

  handleRangeChange = newRange => {
    this.handleStartDateChange(newRange.startDate);
    this.handleEndDateChange(newRange.endDate);
  }

  handleOpen = event => {
     this.setState({
       anchorEl: event.currentTarget,
     });
   };

   getDateRange = () => {
     return {
      startDate: this.state.startDate,
      endDate: this.state.endDate
    }
  };

   handleClose = () => {
     this.setState({
       anchorEl: null,
     });

     this.props.onDateRangeChange(this.getDateRange());
   };

   render() {
     const { classes } = this.props;
     const { anchorEl } = this.state;
     const isOpen = Boolean(anchorEl);

     const dateRange = this.getDateRange();

     return (
       <div>
         <DateRangeInput
           dateRange={dateRange}
           onClick={this.handleOpen}
         />
         <Popover
           id="date-range-popover"
           open={isOpen}
           anchorEl={anchorEl}
           onClose={this.handleClose}
           anchorOrigin={{
             vertical: 'bottom',
             horizontal: 'center',
           }}
           transformOrigin={{
             vertical: 'top',
             horizontal: 'center',
           }}
           >
           <DateRangePickerPanel
              dateRange={dateRange}
              onRangeChange={this.handleRangeChange}
              onStartDateChange={this.handleStartDateChange}
              onEndDateChange={this.handleEndDateChange}
              className={classes.popoverContent}/>
         </Popover>
       </div>
     );
   }
}

const mapStateToProps = state => {
  return {
    dateRange: parseRange(getFilterDateRange(state))
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDateRangeChange: dateRange => {
      dispatch(changeDateInterval(dateRange))
      //TODO reconsider when to use fetching
      dispatch(fetchTransactions())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DateRangePicker));
