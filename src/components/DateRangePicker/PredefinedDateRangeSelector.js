import React from 'react';

import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import CurrentMonthItem from './predefined/CurrentMonthItem';
import PreviousMonthItem from './predefined/PreviousMonthItem';
import CurrentYearItem from './predefined/CurrentYearItem';
import PreviousYearItem from './predefined/PreviousYearItem';

const PredefinedDateRangeSelector = (props) => {

  const { onRangeChange, classes } = props;

  const currentDate = new Date();

  //TODO configurable item with json config?

  return(
      <List disablePadding>
        <CurrentMonthItem currentDate={currentDate} onClick={onRangeChange} />
        <PreviousMonthItem currentDate={currentDate} monthsBack={1} onClick={onRangeChange}/>
        <PreviousMonthItem currentDate={currentDate} monthsBack={2} onClick={onRangeChange}/>
        <Divider />
        <CurrentYearItem currentDate={currentDate} onClick={onRangeChange}/>
        <PreviousYearItem currentDate={currentDate} yearsBack={1} onClick={onRangeChange}/>
      </List>
  );
}

export default PredefinedDateRangeSelector;
