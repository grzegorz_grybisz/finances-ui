import React, { PureComponent } from 'react';
import { createMuiTheme, MuiThemeProvider, withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import PredefinedDateRangeSelector from './PredefinedDateRangeSelector';
import RangeCalendar from './RangeCalendar';

import { primaryBackgroundTheme } from '../../themes/themes.js';


class DateRangePickerPanel extends PureComponent {

  render() {
    const { dateRange, onRangeChange, onStartDateChange, onEndDateChange } = this.props;

    return (
      <Paper style={{ overflow: 'hidden'}}>
        <Grid container>
          <MuiThemeProvider theme={primaryBackgroundTheme}>
            <Grid item>
              <PredefinedDateRangeSelector onRangeChange={onRangeChange}/>
            </Grid>
          </MuiThemeProvider>
          <Grid item>
            <RangeCalendar
              dateRange={dateRange}
              onStartDateChange={onStartDateChange}
              onEndDateChange={onEndDateChange}
            />
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default DateRangePickerPanel;
