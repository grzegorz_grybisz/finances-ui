import React from 'react';
import { format } from 'date-fns'

import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import DateRangeIcon from '@material-ui/icons/DateRange';

const styles = {
  root: {
    width: 250,
  },
  iconButton: {
    padding: 10,
  },
};

const DateRangeInput = (props) => {
  const { dateRange, onClick, classes} = props;

  const value = rangeToString(dateRange);

  return (
     <Input
        className={classes.root}
        value={value}
        onClick={onClick}
        fullWidth
        readOnly
        endAdornment={
           <InputAdornment position="end">
             <DateRangeIcon  color="primary"/>
           </InputAdornment>
         }
     />
   );
}

const rangeToString = dateRange => {
  const from = formatDate(dateRange.startDate);
  const to = formatDate(dateRange.endDate);

  return from + " - "+ to;
}

const formatDate = date => {
  return format(date, "dd MMM yyyy")
}

export default withStyles(styles)(DateRangeInput);
