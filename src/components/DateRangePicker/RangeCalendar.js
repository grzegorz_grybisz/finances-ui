import React, { PureComponent } from 'react';
import { Calendar } from '@material-ui/pickers';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const styles = {
  calendar: {
    overflow: 'hidden',
  },
};

const RangeCalendar = (props) => {

  const {dateRange, onStartDateChange, onEndDateChange} = props;
  const { classes } = props;

  return (
    <Grid container direction="row" spacing={2}>
      <Grid item className={classes.calendar}>
        <Calendar date={dateRange.startDate} onChange={onStartDateChange} />
      </Grid>
      <Grid item className={classes.calendar}>
        <Calendar date={dateRange.endDate} onChange={onEndDateChange} />
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(RangeCalendar);
