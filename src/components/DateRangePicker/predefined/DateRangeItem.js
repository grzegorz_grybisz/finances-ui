import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const DateRangeItem = (props) => {

  const { label, onClick } = props;

  return(
    <ListItem button onClick={onClick}>
      <ListItemText primary={label} />
    </ListItem>
  );
}

export default DateRangeItem;
