import React from 'react';
import { format } from 'date-fns'

import DateRangeItem from './DateRangeItem';
import { getPreviousYear } from '../../../util/DateRangeUtils'

const PreviousYearItem = (props) => {

  const { currentDate, yearsBack, onClick } = props;

  const dateRange = getPreviousYear(currentDate, yearsBack)

  const handleClick = () => {
    onClick(dateRange);
  }

  const label = getYearLabel(dateRange);

  return(
    <DateRangeItem label={label} onClick={handleClick} />
  );
}

const getYearLabel = (dateRange) => {
  return format(dateRange.startDate, 'yyyy');
}

export default PreviousYearItem;
