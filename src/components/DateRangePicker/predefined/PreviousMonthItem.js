import React from 'react';
import { format } from 'date-fns'

import DateRangeItem from './DateRangeItem';
import { getPreviousMonth } from '../../../util/DateRangeUtils'

const PreviousMonthItem = (props) => {

  const { currentDate, monthsBack, onClick } = props;

  const dateRange = getPreviousMonth(currentDate, monthsBack)

  const handleClick = () => {
    onClick(dateRange);
  }

  const label = getMonthLabel(dateRange);

  return(
    <DateRangeItem label={label} onClick={handleClick} />
  );
}

const getMonthLabel = (dateRange) => {
  return format(dateRange.startDate, 'MMMM yyyy');
}

export default PreviousMonthItem;
