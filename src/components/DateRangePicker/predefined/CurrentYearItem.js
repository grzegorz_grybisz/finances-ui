import React from 'react';

import DateRangeItem from './DateRangeItem';
import { getCurrentYear } from '../../../util/DateRangeUtils'

const CurrentYearItem = (props) => {

  const { currentDate, onClick } = props;

  const handleClick = () => {
    onClick(getCurrentYear(currentDate));
  }

  return(
    <DateRangeItem label="This year" onClick={handleClick} />
  );
}

export default CurrentYearItem;
