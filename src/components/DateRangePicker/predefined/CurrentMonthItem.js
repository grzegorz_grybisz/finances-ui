import React from 'react';

import DateRangeItem from './DateRangeItem';
import { getCurrentMonth } from '../../../util/DateRangeUtils'

const CurrentMonthItem = (props) => {

  const { currentDate, onClick } = props;

  const handleClick = () => {
    onClick(getCurrentMonth(currentDate));
  }

  return(
    <DateRangeItem label="This month" onClick={handleClick} />
  );
}

export default CurrentMonthItem;
