import React from 'react';

import { DatePicker } from '@material-ui/pickers';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

class ConfigurableDatePicker extends React.Component{

 handleChange = date => {
   const fieldId = this.props.fieldConfig.id;
   this.props.setFieldValue(fieldId, date);
 }

 render() {
   const { fieldConfig, value, onChange } = this.props;

   return (
       <DatePicker
         id={fieldConfig.id}
         label={fieldConfig.label}
         autoFocus={fieldConfig.autoFocus}
         margin="dense"
         onChange={this.handleChange}
         value={value}
         autoOk
         fullWidth
         leftArrowIcon={<ChevronLeftIcon />}
         rightArrowIcon={<ChevronRightIcon />}
       />
     )
   }
}

export default ConfigurableDatePicker;
