import React from 'react';

import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';

const ConfigurableMenuItem = props =>{
  const {menuItem} = props;

  if(menuItem.checkbox){
    return renderCheckboxItem(menuItem);
  }
  return renderSimpleItem(menuItem);
}

const renderCheckboxItem = menuItem => (
  <MenuItem
    key={menuItem.key}
    onClick={menuItem.onClick}>
      <Checkbox disableRipple checked={menuItem.value()} />
      <ListItemText inset primary={menuItem.label} />
  </MenuItem>
)

const renderSimpleItem = menuItem => (
  <MenuItem key={menuItem.key} onClick={menuItem.onClick}>
    {menuItem.label}
  </MenuItem>
)

export default ConfigurableMenuItem;
