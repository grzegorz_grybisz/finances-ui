import React from 'react'

import LinearProgress from '@material-ui/core/LinearProgress';
import Fade from '@material-ui/core/Fade';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  placeholder: {
    height: 10,
  },
});

const LoadingBar = (props) => {

  const {classes, isLoading} = props;

  return (
    <div className={classes.placeholder}>
      <Fade in={isLoading} style={{transitionDelay: '800ms'}} unmountOnExit>
        <LinearProgress />
      </Fade>
    </div>
  );
}

export default withStyles(styles)(LoadingBar);
