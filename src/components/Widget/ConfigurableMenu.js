import React from 'react'

import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';

import ConfigurableMenuItem from './ConfigurableMenuItem.js'

class ConfigurableMenu extends React.Component{
  state = {
    isOpened: false,
    anchorElement: null
  }

  handleOpen = event => {
    this.setState({ isOpened: true });
    this.setState({ anchorElement: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ isOpened: false });
    this.setState({ anchorElement: null });
  };

  render(){
    const menuAnchorOrigin = {
     horizontal: 'left',
     vertical: 'bottom'
   };

   const config = this.props.config;
   const menuItems = config.menuItems.map(generateMenuItemsFromConfig);

    return(
      <div>
        <IconButton onClick={this.handleOpen}>
          {this.props.children}
        </IconButton>
        <Menu anchorEl={this.state.anchorElement}
              anchorOrigin={menuAnchorOrigin}
              getContentAnchorEl={null}
              open={this.state.isOpened}
              onClose={this.handleClose}>
              {menuItems}
        </Menu>
        </div>
    );
  }
}

const generateMenuItemsFromConfig = menuItem => (
  <ConfigurableMenuItem key={menuItem.key} menuItem={menuItem} />
)

export default ConfigurableMenu;
