import React from 'react'

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import LoadingBar from './LoadingBar';
import ConfigurableMenu from './ConfigurableMenu'

const styles = theme => ({
  frame: {
    padding: theme.spacing(2)
  },
  title: {
  // padding: theme.spacing(2)
  },
  menuButton: {
    // padding: theme.spacing(2)
  }
});

const WidgetFrame = props => {
  return (
    <Paper className={props.classes.frame} elevation={1}>
      <Grid container>
        <Grid item xs={12}>
          <TitleBar classes={props.classes} config={props.config}/>
          <LoadingBar isLoading={props.isLoading} />
        </Grid>
        <Grid item xs={12}>
          {props.children}
        </Grid>
      </Grid>
    </Paper>
  );
}

const TitleBar = props => {
  return (
      <Grid container direction='row' alignItems='center' justify='space-between'>
        <Grid item xs={7}>
          <Typography variant="h6" className={props.classes.title}>
            {props.config.title}
          </Typography>
        </Grid>
        <Grid item xs={1}>
          {prepareMenu(props)}
        </Grid>
      </Grid>
  );
}

const prepareMenu = props =>{
  const {classes, config} = props;

  if(config.menuConfig){
    return (
      <ConfigurableMenu className={props.classes.menuButton} config={props.config.menuConfig}>
        <MoreVertIcon />
      </ConfigurableMenu>
    );
  }
  return null;
}

export default withStyles(styles)(WidgetFrame);
