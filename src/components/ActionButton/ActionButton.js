import React from 'react';
import { Link } from 'react-router-dom';

import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import CreateFromTemplateIcon from '@material-ui/icons/LibraryAdd';

import TooltipedIconButton from './TooltipedIconButton';
import ParametrizedLink from './ParametrizedLink';

const ActionButton = (props) => {

  const {link, queryParams, label, iconName} = props.config;

 	return (
    <ParametrizedLink url={link} {...{queryParams}}>
      <TooltipedIconButton name={label}>
        <Icon name={iconName} />
      </TooltipedIconButton>
    </ParametrizedLink>
  );
}

const Icon = (props) => {
  if(props.name == 'DELETE') {
    return <DeleteIcon/>;
  }
  if(props.name == 'EDIT') {
    return <CreateIcon/>
  }
  if(props.name == 'FROM_TEMPLATE') {
    return <CreateFromTemplateIcon/>
  }
  return null;
}

export default ActionButton;
