import React from 'react'
import {cleanup, render} from 'react-testing-library'
import { BrowserRouter as Router }  from 'react-router-dom';
import ActionButton from './ActionButton'

afterEach(cleanup);

describe('Action Button', () => {
  test.skip('should have proper link when config id is provided', () => {
      const config = {
      }

      const { getByTestId } = render(
        <Router>
          <ActionButton {...config} />
        </Router>
      );

      const link = getByTestId('actionButton').querySelector("Link");
      console.log(link);

      expect(link.to).toBe('asa');
  });
});
