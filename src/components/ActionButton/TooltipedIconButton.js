import React from 'react';

import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const TooltipedIconButton = (props) => {
 	return (
      <Tooltip title={props.name} enterDelay={500}>
        <IconButton color="primary">
          {props.children}
        </IconButton>
      </Tooltip>
  );
}

export default TooltipedIconButton;
