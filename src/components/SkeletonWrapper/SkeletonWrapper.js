import React from 'react';

import Fade from '@material-ui/core/Fade';

const SkeletonWrapper = (props) =>{

  const {children, loaded, placeholder} = props;

  if(loaded){
    return (
      <Fade in={loaded} style={{transitionDelay: '100ms'}} >
        {children}
      </Fade>
    );
  }
  return placeholder;
}

export default SkeletonWrapper;
