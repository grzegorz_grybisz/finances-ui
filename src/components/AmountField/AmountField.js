import React from 'react';

import InputAdornment from '@material-ui/core/InputAdornment';
import { withStyles } from "@material-ui/core/styles";

import ConfigurableTextField from 'Components/ConfigurableTextField/ConfigurableTextField';

const AmountField = (props) => {

  return (
    <ConfigurableTextField
      endAdornment={'PLN'}
      type={'number'}
      {...props}
    />
  );
}

export default AmountField;
