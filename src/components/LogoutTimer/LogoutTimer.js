import React from 'react';
import { connect } from "react-redux";
import { convertTimeToString, fromTimestamp } from '../../util/DateUtils';

import userManager from "../../auth/UserManager";


import CircularTimer from './CircularTimer.js';

class LogoutTimer extends React.Component {
    constructor(props) {
    super(props);

    const tokenExpires = props.getTokenExpires();
    const timeLeft = fromTimestamp(tokenExpires) - new Date();

    this.state = {
      milisecondsLeft: timeLeft,
      timerText: getTimerText(convertTimeToString(timeLeft))
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    const tokenExpires = this.props.getTokenExpires();

    const timeLeft = fromTimestamp(tokenExpires) - new Date();

    this.setState({
      milisecondsLeft: timeLeft,
      timerText: getTimerText(convertTimeToString(timeLeft))
    });
  }

  render() {
    return (
      <CircularTimer
        timeLeft={this.state.milisecondsLeft}
        timerText={this.state.timerText}
        onClick={silentRenew}/>
    );
  }
};

const silentRenew = () => {
//  userManager.signinSilent();
  userManager.signinSilent().then(user => {
    // Nothing to do, handled by oidc-client-js internally
    },
    err => {
        userManager.events._raiseSilentRenewError(err);
      });
}

const getTimerText = timeLeft => {
  return `Logout in ${timeLeft}`;
}

const mapStateToProps = state => {
  return {
    getTokenExpires: () => state.oidc.user.expires_at
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LogoutTimer);
