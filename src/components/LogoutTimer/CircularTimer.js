import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const CircularTimer = (props) => {

  const {timeLeft, timerText, onClick} = props;
  return (
    <Tooltip title={timerText}>
      <IconButton onClick={onClick}>
        <CircularProgress size={30} thickness={5} variant="static" value={normalise(timeLeft)}/>
      </IconButton>
    </Tooltip>
  );
}

const normalise = value => {
  return value * 100 / 900000;
}

//const normalise = maxValue => value => (value) * 100 / (maxValue);

export default CircularTimer;
