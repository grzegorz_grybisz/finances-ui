import React from 'react';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';

const DialogButtons = (props) => {
  const buttons = generateButtons(props.config);

  return (
    <DialogActions>
      {buttons}
    </DialogActions>
  );
}

const generateButtons = (config) => {
  return config.map(generateButton);
}

const generateButton = (buttonConfig) => {

  const type = buttonConfig.submit ? 'submit' : 'button';

  return (
    <Button
      key={buttonConfig.id}
      id={buttonConfig.id}
      color={buttonConfig.color}
      type={type}
      onClick={buttonConfig.onClick}
    >
      {buttonConfig.label}
    </Button>
  );
}

export default DialogButtons;
