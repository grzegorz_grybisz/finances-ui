import React from 'react';
import { connect } from 'react-redux'

import { Formik } from "formik";

import FormContent from './FormContent';

const ConfigurableForm = (props) => {
  const {
    formConfig,
    formValues,
    onSubmit,
   } = props;

    return(
      <Formik
        render={props => <FormContent formConfig={formConfig} {...props} />}
        onSubmit={onSubmit}
        initialValues={formValues}
      />
    );

}

export default ConfigurableForm;
