import React from 'react';

import Button from "@material-ui/core/Button";
import FormControl from '@material-ui/core/FormControl';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';

import EqualGrid from "../../common/layout/EqualGrid";

import ConfigurableDialogField from 'Components/ConfigurableDialogField/ConfigurableDialogField';
import DialogButtons from 'Components/DialogButtons/DialogButtons';

const FormContent = (props) => {

  const {
    formConfig,
     values,
     errors,
     touched,
     handleChange,
     handleSubmit,
     isValid,
     setFieldValue,
     setFieldTouched
   } = props;

  const gridItems = generateGridItems(formConfig.items, props);

  return(
    <form onSubmit={handleSubmit}>
      <DialogTitle>{formConfig.description.title}</DialogTitle>
      <DialogContent>
        <Grid spacing={2} container>
          {gridItems}
        </Grid>
      </DialogContent>
      <DialogButtons config={formConfig.formButtons}/>
    </form>
  );
}


const generateGridItems = (gridItems, props) => {
  return gridItems.map(generateGridItem(props));
}

const generateGridItem = props => {
  return function(gridItem){

    const gridProps = getGridProps(gridItem);
    const children = generateGridItemChildren(gridItem, props);

    return (
        <Grid item {...gridProps} >
          {children}
        </Grid>
    );
  }
}

const getGridProps = (gridItem) => {

  const { items, fields, ...otherProps } = gridItem;

  return {
    item: true,
    ...getContainerGridProps(gridItem),
    ...otherProps
  }
}

const getContainerGridProps = (gridItem) => {
  if(isContainerGrid(gridItem)){
    return {
      container: true,
    }
  }
  return {};
}

const generateGridItemChildren = (gridItem, props) => {
  if(isContainerGrid(gridItem)) {
    return generateGridItems(gridItem.items, props);
  }
  return gridItem.fields.map(generateField(props));
}

const isContainerGrid = (gridItem) => {
  const { items } = gridItem;
  return items && items.length;
}

const generateField = (props) => {
  return function(fieldConfig){

    const {
      formConfig,
       values,
       errors,
       touched,
       handleChange,
       isValid,
       setFieldValue,
       setFieldTouched
     } = props;


    const value = values[fieldConfig.id];

    return (
      <ConfigurableDialogField
        key={fieldConfig.id}
        fieldConfig={fieldConfig}
        value={value}
        onChange={handleChange}
        setFieldValue={setFieldValue}
      />
    )
  }
}

export default FormContent;
