import React from 'react';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogContent from '@material-ui/core/DialogContent';

import DialogButtons from 'Components/DialogButtons/DialogButtons';

const AlertDialog = (props) => {

  const { config } = props;

  return(
    <React.Fragment>
      <Title title={config.description.title} />
      <DialogContent>
        <Description content={config.description.content} />
      </DialogContent>
      <DialogButtons config={config.buttonsConfig}/>
    </React.Fragment>
  );
}

const Title = (props) => {
  if(props.title){
    return (
      <DialogTitle>
        {props.title}
      </DialogTitle>
    )
  }
  return null;
}

const Description = (props) => {
  if(props.content) {
    return (
      <DialogContentText>
        {props.content}
      </DialogContentText>
    )
  }
  return null;
}


export default AlertDialog;
