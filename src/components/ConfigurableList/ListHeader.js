import React from 'react';

import ListSubheader from '@material-ui/core/ListSubheader';
import Divider from '@material-ui/core/Divider';

import SkeletonWrapper from 'Components/SkeletonWrapper/SkeletonWrapper';
import Skeleton from 'react-loading-skeleton';

const ListHeader = props => {
  const {title, listConfig} = props;

	return (
    <React.Fragment>
      <Dividers isThick={listConfig.thickDividers}/>
      <Subheader title={title}/>
    </React.Fragment>
  );
}

const Dividers = ({isThick}) =>{
  if(isThick){
    return (
      <div>
        <Divider light/>
        <Divider light/>
      </div>
    );
  }
  return (<Divider/>);
}

const Subheader = ({title}) => {
  if(title != null){
    return (<ListSubheader component="div">{title}</ListSubheader>);
  }
  return (<Skeleton width={50}/>);
}

export default ListHeader;
