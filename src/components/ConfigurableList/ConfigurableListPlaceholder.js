import React from 'react';

import BasicList from './BasicList';
import ListHeader from './ListHeader';

const ConfigurableListPlaceholder = (props) => {
  const items = preparePlaceholders(props.listConfig);
  return (
    <BasicList listConfig={props.listConfig}>
      {items}
    </BasicList>
  );
}

const preparePlaceholders = listConfig =>{
  if(listConfig.groupBy){
    return prepareGroupedPlaceholders(listConfig);
  }
  return prepareBasePlaceholders(listConfig, 5);
}

const prepareGroupedPlaceholders = (listConfig) => {
  const ListItemWrapper = listConfig.listItemWrapper;

  var items = [];

  items.push(<ListHeader key={0} title={null} listConfig={listConfig}/>)
  items.push(<ListItemWrapper key={1} content={null}/>)
  items.push(<ListItemWrapper key={2} content={null}/>)
  items.push(<ListHeader key={3} title={null} listConfig={listConfig}/>)
  items.push(<ListItemWrapper key={4} content={null}/>)
  items.push(<ListItemWrapper key={5} content={null}/>)
  items.push(<ListHeader key={6} title={null} listConfig={listConfig}/>)
  items.push(<ListItemWrapper key={7} content={null}/>)
  items.push(<ListItemWrapper key={8} content={null}/>)

  return items
}

const prepareBasePlaceholders = (listConfig, count) => {
  const ListItemWrapper = listConfig.listItemWrapper;

  var items = [];

  for(var i=0; i < count; i++){
    items.push(<ListItemWrapper key={i} content={null}/>)
  }

  return items;
}

export default ConfigurableListPlaceholder;
