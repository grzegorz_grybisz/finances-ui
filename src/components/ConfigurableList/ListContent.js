import React from 'react';

const ListContent = (props) =>{

  const {listConfig, children} = props;

  const wrappedItems = wrapItemsInWrapperComponent(children, listConfig);
  // if(!wrappedItems.length){
  //   return preparePlaceholderItems(listConfig);
  // }

  return (
    <React.Fragment>
      {wrappedItems}
    </React.Fragment>
  );
}

const wrapItemsInWrapperComponent = (items, listConfig) => {
  const ListItemWrapper = listConfig.listItemWrapper;

  return items.map(item => <ListItemWrapper key={item.id} content={item} />);
}


const preparePlaceholderItems = listConfig => {
  const ListItemWrapper = listConfig.listItemWrapper;

  var items = [];

  for(var i=0; i < 3; i++){
    items.push(<ListItemWrapper key={i} content={null}/>)
  }

  return items;
}


export default ListContent;
