import React from 'react';

import BasicList from './BasicList';
import ListHeader from './ListHeader';
import ConfigurableListPlaceholder from './ConfigurableListPlaceholder';
import SkeletonWrapper from 'Components/SkeletonWrapper/SkeletonWrapper';

import { groupBy } from '../../util/ArrayUtils';

const ConfigurableList = (props) => {

  if(!props.children.length){
    return (<ConfigurableListPlaceholder {...props}/>);
  }

  const items = prepareItems(props);

  return (
    <BasicList listConfig={props.listConfig}>
      {items}
    </BasicList>
  );
}

const prepareItems = (props) =>{
    const {listConfig, filterText, children} = props;

    if(listConfig.groupBy){
      const groupsOfItems = groupBy(children, listConfig.groupBy);
      return prepareItemsWithSubheaders(groupsOfItems, listConfig);
    }
    return prepareBaseItems(children, listConfig);
}

const prepareItemsWithSubheaders = (groups, listConfig) => {
  var allItems = [];

  for(var groupIndex in groups){
    if(groups.hasOwnProperty(groupIndex)){

      allItems.push(
          <ListHeader key={groupIndex} title={groupIndex} listConfig={listConfig}/>
      )

      const group = groups[groupIndex];
      const baseItems = prepareBaseItems(group, listConfig);
      for(var i in baseItems){
          const it = baseItems[i];
        allItems.push(it);
      }
    }
  }

  return allItems;
}

const prepareBaseItems = (items, listConfig) => {
  const ListItemWrapper = listConfig.listItemWrapper;

  if(items.length){
    return items.map(item => <ListItemWrapper key={item.id} content={item} />);
  }
}

export default ConfigurableList;
