import React from 'react';

import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';

const BasicList = (props) =>{

  const {classes, listConfig, children} = props;

  const listItems = configureListItems(children, listConfig);

  return (
      <List dense={listConfig.isDense}>
        {listItems}
      </List>
  );
}

const configureListItems = (items, listConfig) => {
  return items.map(item => configureListItem(item, listConfig));
}

const configureListItem = (item, listConfig) => {
  const divider = getDivider(listConfig.hasItemDividers);

  return (
    <div key={item.key}>
      {item}
      {divider}
    </div>
  );
}

const getDivider = showDivider =>{
  if(showDivider){
    return (<Divider light/>);
  }
  return null;
}

export default BasicList;
