import React from 'react';
import {
	HashRouter as Router,
	Route,
	Link,
	Switch
} from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import TimelineIcon from '@material-ui/icons/ShowChart';

import TopBar from './navigation/TopBar';
import SideBar from './navigation/SideBar';
import TimelineDashboard from './timeline/TimelineDashboard';
import TransactionsDashboard from './transactions/TransactionsDashboard';
import TemplatesDashboard from './templates/TemplatesDashboard';
import SettingsDashboard from './settings/SettingsDashboard';

const sidebarWidth = 180;

const useStyles = makeStyles(theme => ({
	root: {
     // overflow: 'auto',
    // position: 'relative',
		// minHeight: '100%',
	height: '100%',
  	display: 'flex',
  },
	appBar: {
		backgroundColor: theme.palette.background.default,
		[theme.breakpoints.up('sm')]: {
			width: `calc(100% - ${sidebarWidth}px)`,
			marginLeft: sidebarWidth,
		},
	},
	sideBar: {
		[theme.breakpoints.up('sm')]: {
			width: sidebarWidth,
			flexShrink: 0,
		  },
	},
	content: {
		flexGrow: 1,
		// overlow: 'auto',
		//height: '100%'
		backgroundColor: theme.palette.background.default,
		[theme.breakpoints.up('sm')]: {
			padding: theme.spacing(4),
		},
	},
	toolbar: theme.mixins.toolbar,
}));

const dashboardConfig = [
	{
		label: 'Timeline',
		path: '/timeline',
		icon: <TimelineIcon color='primary'/>,
		component: TimelineDashboard,
	},
	{
		label: 'Transactions',
		path: '/transactions',
		icon: <TimelineIcon color='primary'/>,
		component: TransactionsDashboard,
	},
	{
		label: 'Templates',
		path: '/templates',
		icon: <TimelineIcon color='primary'/>,
		component: TemplatesDashboard,
	},
	{
		label: 'Settings',
		path: '/settings',
		icon: <TimelineIcon color='primary'/>,
		component: SettingsDashboard,
	}
];

export default function AppPage() {
	const classes = useStyles();

	const [mobileOpen, setMobileOpen] = React.useState(false);

	const handleSidebarToggle = () => {
	  setMobileOpen(!mobileOpen);
	};

	return(
		<div className={classes.root}>
			<TopBar 
				className={classes.appBar}
				handleSidebarToggle={handleSidebarToggle}
			/>
			<SideBar 
				className={classes.sideBar}
				sidebarOpen={mobileOpen}
				handleSidebarToggle={handleSidebarToggle}
			/>
			<main className={classes.content}>
				<div className={classes.toolbar}/>
				<div>
					{mapToRoutes(dashboardConfig)}
				</div>
			</main>
		</div>
	);
}

const mapToRoutes = (dashboardItems) => {
	return dashboardItems.map(mapToRoute);
}

const mapToRoute = item => {
	return <Route key={item.label} path={item.path} component={item.component}/>
}
