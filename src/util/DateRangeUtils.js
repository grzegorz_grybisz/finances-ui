import {
  startOfMonth,
  lastDayOfMonth,
  subMonths,
  startOfYear,
  lastDayOfYear,
  subYears,
  format,
  parseISO,
} from 'date-fns'

export const getCurrentMonth = (date) => {
  return getMonthStartAndEndDates(date);
}

export const getPreviousMonth = (date, monthsBack) => {
  const dateInPreviousMonth = subMonths(date, monthsBack);
  return getMonthStartAndEndDates(dateInPreviousMonth);
}

const getMonthStartAndEndDates = (date) => {
  return {
    startDate: startOfMonth(date),
    endDate: lastDayOfMonth(date)
  }
}

export const getCurrentYear = (date) => {
  return getYearStartAndEndDates(date);
}

export const getPreviousYear = (date, yearBack) => {
  const dateInPreviousYear = subYears(date, yearBack);
  return getYearStartAndEndDates(dateInPreviousYear);
}

const getYearStartAndEndDates = (date) => {
  return {
    startDate: startOfYear(date),
    endDate: lastDayOfYear(date)
  }
}

export const convertToString = (interval) => {
  return {
    startDate: format(interval.startDate, "yyyy-MM-dd"),
    endDate: format(interval.endDate, "yyyy-MM-dd")
  }
}

export const parseRange = (interval) => {
  return {
    startDate: parseISO(interval.startDate),
    endDate: parseISO(interval.endDate)
  }
}
