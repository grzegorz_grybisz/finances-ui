export const getDeploymentLocation = deployedOn =>{
  if(deployedOn){
    return deployedOn;
  }
  return 'UNKNOWN';
}
