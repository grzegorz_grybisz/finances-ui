import {convertDateToString, convertStringToDate} from './DateUtils';

export function convertToDto(formData) {
  return {
    ...formData,
    tags: convertSelectedTagsToArray(formData.tags),
    date: convertDateToString(formData.date),
    wallet: convertSelectedOptionToValue(formData.wallet),
    category: convertSelectedOptionToValue(formData.category),
  }
}

export function convertSelectedTagsToArray(tags) {
  return tags.map(extractTagValue);
}

const convertSelectedOptionToValue = selectedOption => {
  return selectedOption.value;
}

export const convertReduxStateToData = state =>{
  return {
    categoriesData: convertCategoriesToSelectData(state.categories),
    tagsData: convertTagsToSelectData(state.tags),
    walletsData: convertWalletsToSelectData(state.wallets),
    templatesData: convertTemplatesToSelectData(state.templates),
    defaultDate: convertStringToDate(state.transactions.lastCreatedOn),
  }
}


function extractTagValue(tag){
  return tag.value;
}

export function convertCategoriesToSelectData(categories){
  return {
    items: categories.items.map(mapCategoryGroupToSelectItem)
  }
}

const mapCategoryGroupToSelectItem = categoryGroup => {
  return {
    label: categoryGroup.name,
    options: categoryGroup.categories.map(mapCategoryToSelectItem)
  }
}

const mapCategoryToSelectItem = category => {
  return {
    label: category.name,
    value: category.id
  }
}

export function convertTagsToSelectData(tags){
  return {
    items: convertTagsToSelectOptions(tags.items)
  }
}

export const convertTagsToSelectOptions = tags => {
  return tags.map(mapTagToSelectOption);
}

const mapTagToSelectOption = tag => {
  return {
    value: tag,
    label: tag
  }
}

export const convertWalletsToSelectData = wallets => {
  return {
    options: wallets.items.map(convertWalletToSelectOption),
    defaultOption: convertWalletToSelectOption(wallets.defaultWallet)
  }
}

const convertWalletToSelectOption = wallet => {
  if(wallet) {
    return {
      value: wallet.id,
      label: wallet.name,
    }
  }
  return null;
}

const convertTemplatesToSelectData = templates => {
  return {
    options: templates.items.map(convertTemplateToSelectOption)
  }
}

const convertTemplateToSelectOption = template => {
  return {
    value: template.id,
    label: template.name,
    transaction: convertTransactionDataToSelectOption(template.transaction)
  }
}

const convertTransactionDataToSelectOption = transaction => {
  return {
    ...transaction,
    tags: convertTagsToSelectOptions(transaction.tags)
  }
}

export const findPreselectedOption = (options, value) => {
  if(Array.isArray(value)){
    return value;
  }

const foundOption = findOptionByValue(options, value);
  if(foundOption){
    return foundOption;
  }
  return "";
}

const findOptionByValue = (options, value) => {
  for(var index in options){
    var option = options[index];

    if(optionMatchesByValue(option, value)){
      return option;
    }

    if(option.options){
      var foundOption = findOptionByValue(option.options, value);
      if(foundOption){
        return foundOption;
      }
    }
  }
  return null;
}

const optionMatchesByValue = (option, value) => {
  if(option.value){
    return option.value === value;
  }
  return false;
}
