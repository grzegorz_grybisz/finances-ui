export const chooseRandomFrom = (options) => {
  return options[Math.floor((Math.random()*options.length))];
}

export const chooseRandomBetween = (min, max) => {
  return Math.floor(Math.random()*(max-min+1)+min);
}
