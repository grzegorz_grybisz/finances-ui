import {
  format,
  parseISO,
  formatDistanceStrict,
  fromUnixTime,

} from 'date-fns'

export function convertDateToString(date) {
  return format(date, "yyyy-MM-dd");
}

export function convertStringToDate(dateAsString){
  if(dateAsString){
    return parseISO(dateAsString);
  }
  return new Date();
}

export const fromTimestamp = timestamp => {
  return fromUnixTime(timestamp);
}

export const convertTimeToString = time => {
  return format(time, "mm:ss");
}
