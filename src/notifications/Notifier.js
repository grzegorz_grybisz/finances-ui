import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withSnackbar } from 'notistack'
import { removeSnackbar } from '../store/snackbars/action';

class Notifier extends Component {
  displayed = [];

  storeDisplayed = id => {
      this.displayed = [...this.displayed, id];
  };

  // TODO
  // Verify performance issues and try to amend them with PureComponent
  // If that wont work, try custom code below

  // shouldComponentUpdate({ notifications: newSnacks = [] }) {
  //   const { notifications: currentSnacks } = this.props;
  //   let notExists = false;
  //   for (let i = 0; i < newSnacks.length; i++) {
  //       if (notExists) continue;
  //       notExists = notExists || !currentSnacks.filter(({ key }) => newSnacks[i].key === key).length;
  //   }
  //   return notExists;
  // }

  componentDidUpdate() {
    const { notifications = [] } = this.props;

    console.log("NOTIFIER UPDATED!", notifications);

    notifications.forEach(notification => {
        // Do nothing if snackbar is already displayed
        if (this.displayed.includes(notification.key)) return;
        // Display snackbar using notistack
        this.props.enqueueSnackbar(notification.message, notification.options);
        // Keep track of snackbars that we've displayed
        this.storeDisplayed(notification.key);
        // Dispatch action to remove snackbar from redux store
        this.props.removeSnackbarFromState(notification.key);
    });
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    notifications: state.snackbars.notifications
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeSnackbarFromState: key => {
      dispatch(removeSnackbar(key))
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(withSnackbar(Notifier));
