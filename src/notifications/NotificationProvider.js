import React from 'react';
import { SnackbarProvider } from 'notistack';

import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  closeIcon: {
    fontSize: 20,
  },
  variantIcon: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
});

const NotificationProvider = (props) => {

  const dismissButton = createDismissButton(props);
  const variantIcons = createVariantIcons(props);

  return (
    <SnackbarProvider iconVariant={variantIcons} action={[dismissButton]}>
      {props.children}
    </SnackbarProvider>
  )
}

const createDismissButton = (props) => {
  return (
    <IconButton key="close" color="inherit">
      <CloseIcon className={props.classes.closeIcon}/>
    </IconButton>
  )
}

const createVariantIcons = (props) => {
  return {
    success: <CheckCircleIcon className={props.classes.variantIcon}/>,
    warning: <WarningIcon className={props.classes.variantIcon}/>,
    error: <ErrorIcon className={props.classes.variantIcon}/>,
    info: <InfoIcon className={props.classes.variantIcon}/>,
  };
}

export default withStyles(styles)(NotificationProvider);
