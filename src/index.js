require('./css/main.css');

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { OidcProvider } from 'redux-oidc';
import { PersistGate } from 'redux-persist/integration/react'

import App from './App.js';
import SplashPage from './SplashPage'
import { store, persistor} from './store/store.js'
import userManager from './auth/UserManager';

ReactDOM.render(
  <Provider store={store}>
    <OidcProvider store={store} userManager={userManager}>
      <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
    </OidcProvider>
  </Provider>,
  document.getElementById('app')
);
