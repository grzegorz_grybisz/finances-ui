import React from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import WelcomeFrame from './login/WelcomeFrame';

import userManager from "./auth/UserManager";

const styles = theme => ({
	background: {
  // backgroundColor: 'rgb(0, 0, 0)',
   overflow: 'hidden',
    backgroundImage: `url(${"/assets/img/welcome-background_a.png"})`,
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: "100%",
   	width: "100%"
  },
  centered: {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  }
})

class LoginPage extends React.Component {
  onLoginButtonClick = event => {
    event.preventDefault();
    userManager.signinRedirect();
  }

  render(){
    return (
      <div className={this.props.classes.background}>
        <div className={this.props.classes.centered}>
          <WelcomeFrame>
            <Button  variant="contained" onClick={this.onLoginButtonClick}>
              Login
            </Button>
          </WelcomeFrame>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(LoginPage);
