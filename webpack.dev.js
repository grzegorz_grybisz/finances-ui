const merge = require('webpack-merge');
const webpack = require('webpack')
const common = require('./webpack.common.js');

const definePlugin = new webpack.DefinePlugin({
  'FINANCES_HOST': JSON.stringify('http://localhost:8080'),
  'TEMPLATES_HOST': JSON.stringify('http://localhost:8081'),
  'DEPLOYED_ON': JSON.stringify('local'),
})

module.exports = merge(common, {
 mode: 'development',
 plugins: [definePlugin]
});
