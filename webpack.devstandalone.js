const merge = require('webpack-merge');
const webpack = require('webpack')
const common = require('./webpack.common.js');

const definePlugin = new webpack.DefinePlugin({
  'FINANCES_HOST': JSON.stringify('https://transactions-service-initial.herokuapp.com'),
  'TEMPLATES_HOST': JSON.stringify('https://templates-service.herokuapp.com'),
  'DEPLOYED_ON': JSON.stringify('local'),
})

module.exports = merge(common, {
 mode: 'development',
 plugins: [definePlugin]
});
