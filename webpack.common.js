const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const path = require('path')

const htmlPlugin = new HtmlWebpackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

const definePlugin = new webpack.DefinePlugin({
  'VERSION': JSON.stringify(require("./package.json").version)
})

module.exports = {
    plugins: [htmlPlugin, definePlugin],
    output: {
      publicPath: '/'
    },
    devServer: {
    	host: '127.0.0.1',
    	port: 4040,
      historyApiFallback: true,
    },
    resolve: {
     alias: {
       Components: path.resolve(__dirname, 'src/components/'),
     }
    },
    module: {
    	rules: [
    	{
    		test: /\.css$/,
    		use: ['style-loader','css-loader']
    	},
    	{
    		test: /\.js$/,
        	exclude: /(node_modules|bower_compontents)/,
    		use: {
          		loader: 'babel-loader'
        	}
    	},
    	{
    		test: /\.png$/,
    		loader: "url-loader?limit=100000"
    	},
    	{
    		test: /\.jpg$/,
    		loader: "file-loader"
    	},
    	{
    		test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
    		loader: 'url-loader? limit=10000&mimetype=application/font-woff'
    	},
    	{
    		test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    		loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
    	},
    	{
    		test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    		loader: 'file-loader'
    	},
    	{
    		test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    		loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
    	}
    	]
    }
};
